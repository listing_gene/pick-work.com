<?php get_header(); ?>

<div class="slider-img slider-header">
	<div><img src="<?php bloginfo('template_url'); ?>/img/header_pick.jpg" alt="厚生労働省認可業界初求人のマッチングサービス。女の子の為のラウンジ求人。ピック"></div>
	<div><a onclick="return gtag_report_conversion('https://aima-match.com/cast_phwtmj/')"><img src="<?php bloginfo('template_url'); ?>/img/header_pick2.png" alt="ギャラ飲みするならaima。身バレの心配なし。登録した日から参加できる。1日20万稼げた実績あり。"></a></div>
	<div><a onclick="return gtag_report_conversion('https://pick-work.com/introduction')"><img src="<?php bloginfo('template_url'); ?>/img/header_picknight.jpg" alt="全国ナイトワーク対応！pickなら友達紹介で最大30万円！紹介でお得に稼げる！会員制ラウンジ求人サイト。業界初求人マッチングサービス"></a></div>
	<div><a onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img src="<?php bloginfo('template_url'); ?>/img/header_campaign.jpg" alt="お友達紹介キャンペーン。入店お祝い金２万円プレゼント。紹介した方と、お友達にそれぞれ１万円ずつプレゼント。"></a></div>
	<div><a onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img src="<?php bloginfo('template_url'); ?>/img/header_safety.jpg" alt="初めての方でも安心、安全。あなたに合ったナイトワークをご提案。面接対策や同行など、0〜10まで完全サポート致します。"></a></div>
	<div><a onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img src="<?php bloginfo('template_url'); ?>/img/header_point.jpg" alt="ピックのおすすめポイント。エリア専門スタッフの専属対応。時給交渉500〜2,000円UP。提携店舗は、なんと500店舗以上。待遇が良いからリピート率93.4％。未経験者大歓迎。いつでもご気軽にご相談下さい。"></a></div>
	<!-- <div><a onclick="return gtag_report_conversion('https://recta-scout.com/?i=osWT7')"><img src="<?php bloginfo('template_url'); ?>/img/img_recta.jpg" alt=""></a></div> -->
</div>

<div class="main">
	<!-- Xバナー -->
    <div class="header_area_xbanner_pc">
		<a href="http://x-lounge.tokyo/cast_phvm4c" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/X_banner2.png" class="x_banner" alt=""></a>
	</div>
	<!-- pickup店舗 -->
	<!-- pickup店舗コンテンツヘッダー（PC) -->
	<div class="contents_header_area_pc">
		<h2 class="cth_word">pickup店舗</h2>
	</div>
	<!-- pickup店舗コンテンツヘッダー（スマホ) -->
	<div class="cth cth_pickup">
		<img class="contents-header" src="<?php bloginfo('template_url'); ?>/img/cth_back.png" alt="">
		<h2 class="cth_word">pickup店舗</h2>
	</div>

	<div class="slider_tenpo">
		<?php $pickup = array(
			'posts_per_page'   => 5, //表示する数
			'post_type'    => 'tenpo',  //カスタム投稿名
			'orderby'          => 'date',  //日付順
			'meta_key' => 'tenpo-pickup', //カスタムフィールドのキー
			'meta_value' => true, //カスタムフィールドの値
		);
		$my_query = new WP_Query($pickup);
		if ($my_query->have_posts()) :
			while ($my_query->have_posts()) : $my_query->the_post(); ?>

				<!-- １つ目の店舗 -->
				<?php $count = $my_query->current_post + 1;
				if ($count == 1) : ?>
					<div class="shoplist">

						<!-- PC用 -->
						<div class="shoplist_pc">
							<div class="shoplist_left">
								<!-- 店舗画像 -->
								<a class="top_img" href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
								<!-- １つ目の店舗用LINEバナー -->
								<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
									<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn7.png" alt="求人締め切り間近、業界トップクラスの人気店。詳細についてラインで相談・応募する">
								</a>
							</div>
							<div class="shoplist_right">
								<div class="shop_top">
									<!-- 店舗名 -->
									<div class="shopname">
										<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
									</div>
									<!-- 所属タグ -->
									<div class="terms">
										<?php $terms = get_the_terms($post->ID, 'area');
										foreach ($terms as $term) {
											echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
										} ?>
										<?php $terms = get_the_terms($post->ID, 'industry');
										foreach ($terms as $term) {
											echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
										} ?>
									</div>
								</div>
								<!-- 店舗説明 -->
								<p><?php the_field("tenpo-comment"); ?></p>
								<!-- 基本情報 -->
								<div class="info">
									<ul class="option op01">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
									</ul>
									<ul class="option op02">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
									</ul>
								</div>
								<!-- メリット -->
								<ul id="condition">
									<?php $taxonomy_name = 'condition_details';
									$taxonomys = get_terms($taxonomy_name, $args);
									if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
										foreach ($taxonomys as $taxonomy) : ?>
											<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
											<?php else : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
									<?php endif;
										endforeach;
									endif; ?>
								</ul>
								<!-- 詳細を見る -->
								<div class="more-btn top-more-btn">
									<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
								</div>
							</div>
						</div>

						<!-- スマホ用 -->
						<div class="shoplist_mb">
							<div class="shoplist_top">
								<!-- 店舗名 -->
								<div class="shopname">
									<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
								</div>
								<!-- 所属タグ -->
								<div class="terms">
									<?php $terms = get_the_terms($post->ID, 'area');
									foreach ($terms as $term) {
										echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
									} ?>
									<?php $terms = get_the_terms($post->ID, 'industry');
									foreach ($terms as $term) {
										echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
									} ?>
								</div>
								<!-- 店舗画像 -->
								<a href="<?php the_permalink(); ?>"><img class="shoplist_image_archive" src="<?php the_field("tenpo-img"); ?>" alt="ラウンジイメージ"></a>
							</div>
							<!-- 基本情報 -->
							<div class="info">
								<ul class="option op01">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
								</ul>
								<ul class="option op02">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
								</ul>
							</div>
							<!-- メリット -->
							<ul id="condition">
								<?php $taxonomy_name = 'condition_details';
								$taxonomys = get_terms($taxonomy_name, $args);
								if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
									foreach ($taxonomys as $taxonomy) :
								?>
										<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
										<?php else : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
								<?php endif;
									endforeach;
								endif; ?>
							</ul>
							<!-- 応募ボタン -->
							<div class="top_apply">
								<!-- １つ目の店舗用LINEバナー -->
								<div class="top_apply_line">
									<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
										<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn7.png" alt="求人締め切り間近、業界トップクラスの人気店。詳細についてラインで相談・応募する">
									</a>
								</div>
							</div>
							<!-- 詳細を見る -->
							<div class="more-btn top-more-btn">
								<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
							</div>
						</div>

					</div>

					<!-- ２つ目の店舗 -->
				<?php elseif ($count == 2) : ?>
					<div class="shoplist">

						<!-- PC用 -->
						<div class="shoplist_pc">
							<div class="shoplist_left">
								<!-- 店舗画像 -->
								<a class="top_img" href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
								<!-- ２つ目の店舗用LINEバナー -->
								<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
									<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn3.png" alt="人気地域の超人気高級店。詳細についてラインで相談・応募する">
								</a>
							</div>
							<div class="shoplist_right">
								<div class="shop_top">
									<!-- 店舗名 -->
									<div class="shopname">
										<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
									</div>
									<!-- 所属タグ -->
									<div class="terms">
										<?php $terms = get_the_terms($post->ID, 'area');
										foreach ($terms as $term) {
											echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
										} ?>
										<?php $terms = get_the_terms($post->ID, 'industry');
										foreach ($terms as $term) {
											echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
										} ?>
									</div>
								</div>
								<!-- 店舗説明 -->
								<p><?php the_field("tenpo-comment"); ?></p>
								<!-- 基本情報 -->
								<div class="info">
									<ul class="option op01">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
									</ul>
									<ul class="option op02">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
									</ul>
								</div>
								<!-- メリット -->
								<ul id="condition">
									<?php $taxonomy_name = 'condition_details';
									$taxonomys = get_terms($taxonomy_name, $args);
									if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
										foreach ($taxonomys as $taxonomy) : ?>
											<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
											<?php else : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
									<?php endif;
										endforeach;
									endif; ?>
								</ul>
								<!-- 詳細を見る -->
								<div class="more-btn top-more-btn">
									<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
								</div>
							</div>
						</div>

						<!-- スマホ用 -->
						<div class="shoplist_mb">
							<div class="shoplist_top">
								<!-- 店舗名 -->
								<div class="shopname">
									<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
								</div>
								<!-- 所属タグ -->
								<div class="terms">
									<?php $terms = get_the_terms($post->ID, 'area');
									foreach ($terms as $term) {
										echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
									} ?>
									<?php $terms = get_the_terms($post->ID, 'industry');
									foreach ($terms as $term) {
										echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
									} ?>
								</div>
								<!-- 店舗画像 -->
								<a href="<?php the_permalink(); ?>"><img class="shoplist_image_archive" src="<?php the_field("tenpo-img"); ?>" alt="ラウンジイメージ"></a>
							</div>
							<!-- 基本情報 -->
							<div class="info">
								<ul class="option op01">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
								</ul>
								<ul class="option op02">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
								</ul>
							</div>
							<!-- メリット -->
							<ul id="condition">
								<?php $taxonomy_name = 'condition_details';
								$taxonomys = get_terms($taxonomy_name, $args);
								if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
									foreach ($taxonomys as $taxonomy) :
								?>
										<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
										<?php else : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
								<?php endif;
									endforeach;
								endif; ?>
							</ul>
							<!-- 応募ボタン -->
							<div class="top_apply">
								<!-- ２つ目の店舗用LINEバナー -->
								<div class="top_apply_line">
									<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
										<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn3.png" alt="人気地域の超人気高級店。詳細についてラインで相談・応募する">
									</a>
								</div>
							</div>
							<!-- 詳細を見る -->
							<div class="more-btn top-more-btn">
								<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
							</div>
						</div>

					</div>

					<!-- ３つ目の店舗 -->
				<?php elseif ($count == 3) : ?>
					<div class="shoplist">

						<!-- PC用 -->
						<div class="shoplist_pc">
							<div class="shoplist_left">
								<!-- 店舗画像 -->
								<a class="top_img" href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
								<!-- ３つ目の店舗用LINEバナー -->
								<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
									<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn4.png" alt="在籍だけでもステータスの大手有名店。詳細についてラインで相談・応募する">
								</a>
							</div>
							<div class="shoplist_right">
								<div class="shop_top">
									<!-- 店舗名 -->
									<div class="shopname">
										<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
									</div>
									<!-- 所属タグ -->
									<div class="terms">
										<?php $terms = get_the_terms($post->ID, 'area');
										foreach ($terms as $term) {
											echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
										} ?>
										<?php $terms = get_the_terms($post->ID, 'industry');
										foreach ($terms as $term) {
											echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
										} ?>
									</div>
								</div>
								<!-- 店舗説明 -->
								<p><?php the_field("tenpo-comment"); ?></p>
								<!-- 基本情報 -->
								<div class="info">
									<ul class="option op01">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
									</ul>
									<ul class="option op02">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
									</ul>
								</div>
								<!-- メリット -->
								<ul id="condition">
									<?php $taxonomy_name = 'condition_details';
									$taxonomys = get_terms($taxonomy_name, $args);
									if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
										foreach ($taxonomys as $taxonomy) : ?>
											<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
											<?php else : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
									<?php endif;
										endforeach;
									endif; ?>
								</ul>
								<!-- 詳細を見る -->
								<div class="more-btn top-more-btn">
									<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
								</div>
							</div>
						</div>

						<!-- スマホ用 -->
						<div class="shoplist_mb">
							<div class="shoplist_top">
								<!-- 店舗名 -->
								<div class="shopname">
									<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
								</div>
								<!-- 所属タグ -->
								<div class="terms">
									<?php $terms = get_the_terms($post->ID, 'area');
									foreach ($terms as $term) {
										echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
									} ?>
									<?php $terms = get_the_terms($post->ID, 'industry');
									foreach ($terms as $term) {
										echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
									} ?>
								</div>
								<!-- 店舗画像 -->
								<a href="<?php the_permalink(); ?>"><img class="shoplist_image_archive" src="<?php the_field("tenpo-img"); ?>" alt="ラウンジイメージ"></a>
							</div>
							<!-- 基本情報 -->
							<div class="info">
								<ul class="option op01">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
								</ul>
								<ul class="option op02">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
								</ul>
							</div>
							<!-- メリット -->
							<ul id="condition">
								<?php $taxonomy_name = 'condition_details';
								$taxonomys = get_terms($taxonomy_name, $args);
								if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
									foreach ($taxonomys as $taxonomy) :
								?>
										<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
										<?php else : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
								<?php endif;
									endforeach;
								endif; ?>
							</ul>
							<!-- 応募ボタン -->
							<div class="top_apply">
								<!-- ３つ目の店舗用LINEバナー -->
								<div class="top_apply_line">
									<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
										<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn4.png" alt="在籍だけでもステータスの大手有名店。詳細についてラインで相談・応募する">
									</a>
								</div>
							</div>
							<!-- 詳細を見る -->
							<div class="more-btn top-more-btn">
								<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
							</div>
						</div>

					</div>

					<!-- ４つ目の店舗 -->
				<?php elseif ($count == 4) : ?>
					<div class="shoplist">

						<!-- PC用 -->
						<div class="shoplist_pc">
							<div class="shoplist_left">
								<!-- 店舗画像 -->
								<a class="top_img" href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
								<!-- ４つ目の店舗用LINEバナー -->
								<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
									<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn2.png" alt="超高額時給の老舗。詳細についてラインで相談・応募する">
								</a>
							</div>
							<div class="shoplist_right">
								<div class="shop_top">
									<!-- 店舗名 -->
									<div class="shopname">
										<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
									</div>
									<!-- 所属タグ -->
									<div class="terms">
										<?php $terms = get_the_terms($post->ID, 'area');
										foreach ($terms as $term) {
											echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
										} ?>
										<?php $terms = get_the_terms($post->ID, 'industry');
										foreach ($terms as $term) {
											echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
										} ?>
									</div>
								</div>
								<!-- 店舗説明 -->
								<p><?php the_field("tenpo-comment"); ?></p>
								<!-- 基本情報 -->
								<div class="info">
									<ul class="option op01">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
									</ul>
									<ul class="option op02">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
									</ul>
								</div>
								<!-- メリット -->
								<ul id="condition">
									<?php $taxonomy_name = 'condition_details';
									$taxonomys = get_terms($taxonomy_name, $args);
									if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
										foreach ($taxonomys as $taxonomy) : ?>
											<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
											<?php else : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
									<?php endif;
										endforeach;
									endif; ?>
								</ul>
								<!-- 詳細を見る -->
								<div class="more-btn top-more-btn">
									<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
								</div>
							</div>
						</div>

						<!-- スマホ用 -->
						<div class="shoplist_mb">
							<div class="shoplist_top">
								<!-- 店舗名 -->
								<div class="shopname">
									<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
								</div>
								<!-- 所属タグ -->
								<div class="terms">
									<?php $terms = get_the_terms($post->ID, 'area');
									foreach ($terms as $term) {
										echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
									} ?>
									<?php $terms = get_the_terms($post->ID, 'industry');
									foreach ($terms as $term) {
										echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
									} ?>
								</div>
								<!-- 店舗画像 -->
								<a href="<?php the_permalink(); ?>"><img class="shoplist_image_archive" src="<?php the_field("tenpo-img"); ?>" alt="ラウンジイメージ"></a>
							</div>
							<!-- 基本情報 -->
							<div class="info">
								<ul class="option op01">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
								</ul>
								<ul class="option op02">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
								</ul>
							</div>
							<!-- メリット -->
							<ul id="condition">
								<?php $taxonomy_name = 'condition_details';
								$taxonomys = get_terms($taxonomy_name, $args);
								if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
									foreach ($taxonomys as $taxonomy) :
								?>
										<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
										<?php else : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
								<?php endif;
									endforeach;
								endif; ?>
							</ul>
							<!-- 応募ボタン -->
							<div class="top_apply">
								<!-- ４つ目の店舗用LINEバナー -->
								<div class="top_apply_line">
									<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
										<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn2.png" alt="超高額時給の老舗。詳細についてラインで相談・応募する">
									</a>
								</div>
							</div>
							<!-- 詳細を見る -->
							<div class="more-btn top-more-btn">
								<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
							</div>
						</div>

					</div>

					<!-- ５つ目の店舗 -->
				<?php elseif ($count == 5) : ?>
					<div class="shoplist">

						<!-- PC用 -->
						<div class="shoplist_pc">
							<div class="shoplist_left">
								<!-- 店舗画像 -->
								<a class="top_img" href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
								<!-- ５つ目の店舗用LINEバナー -->
								<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
									<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn6.png" alt="未経験、初心者の方も大歓迎。詳細についてラインで相談・応募する">
								</a>
							</div>
							<div class="shoplist_right">
								<div class="shop_top">
									<!-- 店舗名 -->
									<div class="shopname">
										<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
									</div>
									<!-- 所属タグ -->
									<div class="terms">
										<?php $terms = get_the_terms($post->ID, 'area');
										foreach ($terms as $term) {
											echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
										} ?>
										<?php $terms = get_the_terms($post->ID, 'industry');
										foreach ($terms as $term) {
											echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
										} ?>
									</div>
								</div>
								<!-- 店舗説明 -->
								<p><?php the_field("tenpo-comment"); ?></p>
								<!-- 基本情報 -->
								<div class="info">
									<ul class="option op01">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
									</ul>
									<ul class="option op02">
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
										<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
									</ul>
								</div>
								<!-- メリット -->
								<ul id="condition">
									<?php $taxonomy_name = 'condition_details';
									$taxonomys = get_terms($taxonomy_name, $args);
									if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
										foreach ($taxonomys as $taxonomy) : ?>
											<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
											<?php else : ?>
												<li>
													<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
												</li>
									<?php endif;
										endforeach;
									endif; ?>
								</ul>
								<!-- 詳細を見る -->
								<div class="more-btn top-more-btn">
									<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
								</div>
							</div>
						</div>

						<!-- スマホ用 -->
						<div class="shoplist_mb">
							<div class="shoplist_top">
								<!-- 店舗名 -->
								<div class="shopname">
									<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
								</div>
								<!-- 所属タグ -->
								<div class="terms">
									<?php $terms = get_the_terms($post->ID, 'area');
									foreach ($terms as $term) {
										echo '<a class="side_area" href="' . get_term_link($term->slug, 'area') . '">' . $term->name . '</a>';
									} ?>
									<?php $terms = get_the_terms($post->ID, 'industry');
									foreach ($terms as $term) {
										echo '<a class="side_industry" href="' . get_term_link($term->slug, 'industry') . '">' . $term->name . '</a>';
									} ?>
								</div>
								<!-- 店舗画像 -->
								<a href="<?php the_permalink(); ?>"><img class="shoplist_image_archive" src="<?php the_field("tenpo-img"); ?>" alt="ラウンジイメージ"></a>
							</div>
							<!-- 基本情報 -->
							<div class="info">
								<ul class="option op01">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br(post_custom('top-area')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br(post_custom('top-time')); ?></span></li>
								</ul>
								<ul class="option op02">
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br(post_custom('top-salay')); ?></span></li>
									<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br(post_custom('top-payment')); ?></span></li>
								</ul>
							</div>
							<!-- メリット -->
							<ul id="condition">
								<?php $taxonomy_name = 'condition_details';
								$taxonomys = get_terms($taxonomy_name, $args);
								if (!empty($taxonomys) && !is_wp_error($taxonomys)) :
									foreach ($taxonomys as $taxonomy) :
								?>
										<?php if (has_term($taxonomy->slug, 'condition_details')) : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
										<?php else : ?>
											<li class="">
												<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
											</li>
								<?php endif;
									endforeach;
								endif; ?>
							</ul>
							<!-- 応募ボタン -->
							<div class="top_apply">
								<!-- ５つ目の店舗用LINEバナー -->
								<div class="top_apply_line">
									<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
										<img src="<?php bloginfo('template_url'); ?>/img/pickUpBtn6.png" alt="未経験、初心者の方も大歓迎。詳細についてラインで相談・応募する">
									</a>
								</div>
							</div>
							<!-- 詳細を見る -->
							<div class="more-btn top-more-btn">
								<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
							</div>
						</div>

					</div>
		<?php endif;
			endwhile;
		endif;
		wp_reset_postdata(); ?>
	</div>

	<!-- エリア検索 -->
	<!-- コンテンツヘッダー(PC) -->
	<div class="contents_header_area_pc">
		<h2 class="cth_word">エリア検索</h2>
	</div>
	<!-- コンテンツヘッダー(スマホ) -->
	<div class="cth">
		<img class="contents-header" src="<?php bloginfo('template_url'); ?>/img/cth_back.png" alt="">
		<h2 class="cth_word">エリア検索</h2>
	</div>
	<div class="wrap_area">
		<a href="<?php echo get_term_link('ebisu', 'area') ?>" class="area"><img src="<?php bloginfo('template_url'); ?>/img/img_ebisu.png" alt="恵比寿"></a>
		<a href="<?php echo get_term_link('nisiazabu', 'area') ?>" class="area"><img src="<?php bloginfo('template_url'); ?>/img/img_nisiazabu.png" alt="西麻布"></a>
		<a href="<?php echo get_term_link('roppongi', 'area') ?>" class="area"><img src="<?php bloginfo('template_url'); ?>/img/img_roppongi.png" alt="六本木"></a>
		<a href="<?php echo get_term_link('ginza', 'area') ?>" class="area"><img src="<?php bloginfo('template_url'); ?>/img/img_ginza.png" alt="銀座"></a>
		<a href="<?php echo get_term_link('sinjuku', 'area') ?>" class="area"><img src="<?php bloginfo('template_url'); ?>/img/img_sinjuku.png" alt="新宿"></a>
		<a href="<?php echo get_term_link('ikebukuro', 'area') ?>" class="area"><img src="<?php bloginfo('template_url'); ?>/img/img_ikebukuro.png" alt="池袋"></a>
		<a href="<?php echo get_term_link('other', 'area') ?>" class="other"><img src="<?php bloginfo('template_url'); ?>/img/img_other.png" alt="その他"></a>
		<a href="<?php echo get_term_link('other', 'area') ?>" class="other_pc"><img src="<?php bloginfo('template_url'); ?>/img/img_other_pc.jpg" alt="その他"></a>
	</div>

	<!-- 条件検索 -->
	<!-- コンテンツヘッダー(PC) -->
	<div class="contents_header_area_pc contents_header_area_pc_condition">
		<h2 class="cth_word">条件検索</h2>
	</div>
	<!-- コンテンツヘッダー(スマホ) -->
	<div class="cth cth_search">
		<img class="contents-header" src="<?php bloginfo('template_url'); ?>/img/cth_back.png" alt="">
		<h2 class="cth_word">条件検索</h2>
	</div>
	<!-- 条件検索テンプレートタグ -->
	<?php get_search_form(); ?>
	<!-- LINEバナー、補足テキスト -->
	<div class="top_line">
		<div class="img_btn img_btn_line">
			<a onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img src="<?php bloginfo('template_url'); ?>/img/btn_line_w.png" alt="LINEで応募する"></a>
		</div>
		<p class="line_note">まずはLINE＠でカンタン応募！</p>
		<p>Pick（ピック）に掲載が無いお店でも相談いただければ紹介可能なので一言声をかけて下さい。</p>
	</div>
	<img class="break" src="<?php bloginfo('template_url'); ?>/img/line_break.png">

	<!-- Pickについて -->
	<div class="eyecatching">
		<!-- pickならではの強み -->
		<div class="about_pick">
			<div class="strength">
				<div class="contents_head">
					<!-- <img src="<?php bloginfo('template_url'); ?>/img/title_top.png"> -->
					<h2>Pickならではの強み</h2>
					<!-- <img src="<?php bloginfo('template_url'); ?>/img/title_bottom.png"> -->
				</div>
				<div class="strength-item">
					<img src="<?php bloginfo('template_url'); ?>/img/top_icon_specialty.png">
					<p>エリア専門スタッフが<br>専属サポート</p>
				</div>
				<div class="strength-item">
					<img src="<?php bloginfo('template_url'); ?>/img/top_icon_up.png">
					<p>時給UP交渉<br>500〜3,000円</p>
				</div>
				<div class="strength-item">
					<img src="<?php bloginfo('template_url'); ?>/img/top_icon_license.png">
					<p>厚生労働省認可の<br>ナイトワーク紹介会社</p>
				</div>
				<div class="strength-item">
					<img src="<?php bloginfo('template_url'); ?>/img/top_icon_speed.png">
					<p>即日面接・体入可能<br>スピード対応</p>
				</div>
				<div class="strength-item">
					<img src="<?php bloginfo('template_url'); ?>/img/top_icon_support.png">
					<p>初心者でも安心<br>24時間フルサポート</p>
				</div>
				<div class="strength-item">
					<img src="<?php bloginfo('template_url'); ?>/img/top_icon_pretend.png">
					<p>友だち紹介ボーナス<br>紹介者・友だち共に贈与</p>
				</div>
			</div>
			<!-- pickコメント -->
			<!--
			<div class="pick_support">
				<p>Pickなら<span>各エリア毎の専門スタッフ</span>が対応</p>
				<p>致します！さらに<span>専属スタッフ担当制で0〜10まで</span></p>
				<p><span>完全サポート</span>させて頂きます。</p>
				<p>未経験者、初心者の方には面接対策・同行から</p>
				<p>経験者の方の店舗・業種変更など</p>
				<p><span>即日対応</span>&thinsp;致します！</p>
			</div>
			<div class="pick_support_pc">
				<p>Pickなら<span>各エリア毎の専門スタッフ</span>が対応致します！</p>
				<p>さらに<span>専属スタッフ担当制で0〜10まで完全サポート</span>させて頂きます。</p>
				<p>未経験者、初心者の方には面接対策・同行から経験者の方の店舗・業種変更など</p>
				<p><span>即日対応</span>&thinsp;致します！</p>
			</div> -->

			<!-- なぜ、Pickが選ばれているのか -->
			<a class="img_btn img_btn_lounge" href="<?php echo home_url('/elected'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_elected.png" alt="なぜ、ピックが選ばれているのか">
			</a>
		</div>
	</div>

	<!-- 新着情報 -->
	<!-- コンテンツヘッダー(PC) -->
	<div class="contents_header_area_pc contents_header_area_pc_news">
		<h2 class="cth_word">新着情報</h2>
	</div>
	<div class="info_wrapper">
		<div class="info">
			<!-- コンテンツヘッダー(スマホ) -->
			<div class="cth">
				<img class="contents-header" src="<?php bloginfo('template_url'); ?>/img/cth_back.png" alt="">
				<h2 class="cth_word">新着情報</h2>
			</div>
			<?php
			$args = array(
				'post_type' => 'info', //カスタム投稿名
				'posts_per_page' => 3, // 表示する数
			); ?>
			<?php $my_query = new WP_Query($args); ?>
			<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>

				<!-- ループ開始 -->
				<div class="new_prev">
					<!-- アイキャッチ画像 -->
					<div class="new_img">
						<a href="<?php the_permalink(); ?>"><img src="<?php the_field("info-img"); ?>" alt="<?php the_title(); ?>"></a>
					</div>

					<!-- スマホ用（日付・タイトル・抜粋） -->
					<div class="new_txt">
						<!-- 投稿日付 -->
						<a class="top_date" href="<?php the_permalink(); ?>"><?php the_time('Y.m.d'); ?></a>
						<!-- タイトル -->
						<a class="top_title" href="<?php the_permalink(); ?>">
							<?php if (mb_strlen($post->post_title) > 21) {
								$title = mb_substr($post->post_title, 0, 21);
								echo $title . '...';
							} else {
								echo $post->post_title;
							} ?>
						</a>
						<!-- 本文抜粋 -->
						<a class="top_text" href="<?php the_permalink(); ?>"><?php echo mb_substr(get_the_excerpt(), 0, 26) . '...'; ?></a>
					</div>

					<!-- PC用（日付・タイトル・抜粋） -->
					<div class="new_txt_pc">
						<!-- 投稿日付 -->
						<a class="top_date" href="<?php the_permalink(); ?>"><?php the_time('Y.m.d'); ?></a>
						<!-- タイトル -->
						<a class="top_title" href="<?php the_permalink(); ?>">
							<?php if (mb_strlen($post->post_title) > 37) {
								$title = mb_substr($post->post_title, 0, 37);
								echo $title . '...';
							} else {
								echo $post->post_title;
							} ?>
						</a>
						<!-- 本文抜粋 -->
						<a class="top_text" href="<?php the_permalink(); ?>"><?php echo get_the_custom_excerpt(get_the_content(), 90) . '...' ?></a>
					</div>

				</div>
				<!-- ループ終了 -->
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>
		<!-- もっと見る -->
		<div class="btn_more">
			<a href="<?php echo get_post_type_archive_link('info'); ?>" class="btn_border">もっと見る</a>
		</div>
	</div>

	<!-- コラム -->
	<!-- コンテンツヘッダー(PC) -->
	<div class="contents_header_area_pc contents_header_area_pc_column">
		<h2 class="cth_word">コラム</h2>
	</div>
	<div class="column_wrapper">
		<div class="column">
			<!-- コンテンツヘッダー(スマホ) -->
			<div class="cth">
				<img class="contents-header" src="<?php bloginfo('template_url'); ?>/img/cth_back.png" alt="">
				<h2 class="cth_word">コラム</h2>
			</div>
			<?php
			$args = array(
				'post_type' => 'column', // カスタム投稿名
				'posts_per_page' => 3, // 表示する数
			); ?>
			<?php $my_query = new WP_Query($args); ?>
			<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>

				<!-- ループ開始 -->
				<div class="new_prev_c">
					<!-- アイキャッチ画像 -->
					<div class="new_img_c">
						<a href="<?php the_permalink(); ?>"><img src="<?php the_field("column-img"); ?>" alt="<?php the_title(); ?>"></a>
					</div>

					<!-- スマホ用（タイトル・抜粋） -->
					<div class="new_txt_c">
						<!-- タイトル -->
						<a class="top_title_c" href="<?php the_permalink(); ?>">
							<?php if (mb_strlen($post->post_title) > 21) {
								$title = mb_substr($post->post_title, 0, 21);
								echo $title . '...';
							} else {
								echo $post->post_title;
							} ?>
						</a>
						<!-- 本文抜粋 -->
						<a class="top_text_c" href="<?php the_permalink(); ?>"><?php echo mb_substr(get_the_excerpt(), 0, 40) . '...'; ?></a>
					</div>

					<!-- PC用（タイトル・抜粋） -->
					<div class="new_txt_c_pc">
						<!-- タイトル -->
						<a class="top_title_c" href="<?php the_permalink(); ?>">
							<?php if (mb_strlen($post->post_title) > 35) {
								$title = mb_substr($post->post_title, 0, 35);
								echo $title . '...';
							} else {
								echo $post->post_title;
							} ?>
						</a>
						<!-- 本文抜粋 -->
						<a class="top_text_c" href="<?php the_permalink(); ?>"><?php echo get_the_custom_excerpt(get_the_content(), 100) . '...' ?></a>
					</div>
				</div>
				<!-- ループ終了 -->
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>

		<!-- もっと見る -->
		<div class="btn_more">
			<a href="<?php echo get_post_type_archive_link('column'); ?>" class="btn_border">もっと見る</a>
		</div>
	</div>

	<!-- 各ページリンクバナー -->
	<div id="top_banner-link" class="banner-link top_banner-link">
		<!-- Pickとは -->
		<div class="banner">
			<a href="<?php echo home_url('/about-pick'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/banner_about-pick.png" alt="ピックのサービス内容やメリットなどを、ご紹介します。">
			</a>
			<!-- 利用女性の声 -->
			<a href="<?php echo home_url('/user-voice'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/banner_user-voice.png" alt="実際にピックを利用した方の感想やご意見などを大公開。気になる情報をまとめました。">
			</a>
		</div>
		<div class="banner">
			<!-- 会員制ラウンジとは -->
			<a href="<?php echo home_url('/about'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/banner_lounge.png" alt="隅々まで丸わかり。一般ラウンジと会員制ラウンジの違いやメリットをご紹介します。">
			</a>
			<!-- 応募の手順 -->
			<a href="<?php echo home_url('/step'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/banner_flow.png" alt="応募の手順。専門のスタッフがあなたを徹底サポート。">
			</a>
		</div>
	</div>

	<!-- 応募ボタン -->
	<div class="apply ">
		<div class="apply_line">
			<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_line_w.png" alt="LINEで応募する">
			</a>
		</div>
		<div class="apply_mail">
			<a class="mail-apply" href="<?php echo home_url('/contact'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_mail_w.png" alt="メールで応募する">
			</a>
		</div>
	</div>

	<div class="banner-link banner-link_simple">
		<!-- 下部版バナー -->
		<a class="banner_campaign" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img class="simple-banner" src="<?php bloginfo('template_url'); ?>/img/banner_picknight.jpg" alt="全国ナイトワーク対応！pickなら友達紹介で最大30万円！紹介でお得に稼げる！会員制ラウンジ求人サイト。業界初求人マッチングサービス"></a>
		<a class="banner_campaign" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img class="simple-banner" src="<?php bloginfo('template_url'); ?>/img/banner_campaign.png" alt="お友達紹介キャンペーン。入店お祝い金２万円プレゼント。紹介した方と、お友達にそれぞれ１万円ずつプレゼント。"></a>
		<a class="banner_safety" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img class="simple-banner" src="<?php bloginfo('template_url'); ?>/img/banner_safety.png" alt="初めての方でも安心、安全。あなたに合ったナイトワークをご提案。面接対策や同行など、0〜10まで完全サポート致します。"></a>
		<a onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img class="simple-banner" src="<?php bloginfo('template_url'); ?>/img/banner_point.png" alt="ピックのおすすめポイント。エリア専門スタッフの専属対応。時給交渉500〜2,000円UP。提携店舗は、なんと500店舗以上。待遇が良いからリピート率93.4％。未経験者大歓迎。いつでもご気軽にご相談下さい。"></a>
		<a class="banner_campaign" onclick="return gtag_report_conversion('https://aima-match.com/cast_phwtmj/')"><img class="simple-banner" src="<?php bloginfo('template_url'); ?>/img/banner_pick.png" alt="ギャラ飲みするならaima。身バレの心配なし。登録した日から参加できる。1日20万稼げた実績あり。"></a>
		<!-- <a class="banner_safety" onclick="return gtag_report_conversion('https://recta-scout.com/?i=osWT7')"><img class="simple-banner" src="<?php bloginfo('template_url'); ?>/img/img_recta.jpg" alt=""></a> -->
		<a href="https://bijo-post.com/" target="_blank"><img class="simple-banner" src="<?php bloginfo('template_url'); ?>/img/banner_bjp.png" alt="インスタグラマーを応援するアカウント、美女ポスト。"></a>
	</div>

	<!-- 他サイトバナー -->
	<div class="banner-link banner-site">
		<!-- <a href="https://bijo-post.com/" target="_blank"><img class="simple-banner" src="<?php bloginfo('template_url'); ?>/img/banner_bjp.png" alt="インスタグラマーを応援するアカウント、美女ポスト。"></a> -->
	</div>

	<!-- SNS -->
	<div class="social">
		<p>CHECK</p>
		<div class="social-list">
			<a href="https://www.instagram.com/pick_media"><img src="<?php bloginfo('template_url'); ?>/img/icon_insta2.png" alt="公式Instagram"></a>
			<a href="https://twitter.com/pick_official1"><img src="<?php bloginfo('template_url'); ?>/img/icon_twitter2.png" alt="公式twitter"></a>
		</div>
	</div>
	<!--
		<div class="social">
			<p>公式SNSをチェック</p>
			<div class="links">
			<a href="https://www.instagram.com/pick_media"><img src="<?php bloginfo('template_url'); ?>/img/icon_insta.png" alt="公式Instagram"></a>
			<a href="https://twitter.com/pick_official1"><img src="<?php bloginfo('template_url'); ?>/img/icon_twitter.png" alt="公式twitter"></a>
		</div>
						-->
</div>
</div>

<?php get_footer(); ?>
