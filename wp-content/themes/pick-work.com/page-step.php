<?php get_header(); ?>

<!-- タイトル画像 -->
<div class="page-heading">
    <img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_top.png" alt="">
    <h1>応募の流れ</h1>
    <img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_bottom.png" alt="">
</div>

<div class="step">
    <!-- ステップ１ -->
    <div class="step_item">
        <div class="step_title">
            <div class="step_number">
                <p>STEP.<span>１</span></p>
            </div>
            <div class="step_text">
                <h2>少しでも興味があれば気軽にご相談下さい。</h2>
            </div>
        </div>
        <img src="<?php bloginfo('template_url'); ?>/img/step1.jpg" alt="ステップ１">
        <div class="text">
            <p>未経験で不安な方、経験者でお店を変えたい方など気軽にご相談下さい♪</p>
            <p>時給交渉、面接同行、店舗変更など全て専属スタッフにて個別サポートを致します。</p>
            <p class="notes">※Pick（ピック）に掲載が無いお店でもご雑談頂ければご紹介可能なので一声を掛けて下さい。</p>
            <div class="social_step">
                <div class="step_btn"><a onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
                        <img class="line_btn" src="<?php bloginfo('template_url'); ?>/img/btn_line.png" alt="LINE@">
                    </a></div>
                <div class="step_btn"><a href="http://pick-work.com/contact">
                        <img class="mail_btn" src="<?php bloginfo('template_url'); ?>/img/btn_mail.png" alt="メールで応募">
                    </a></div>
            </div>
        </div>
    </div>
    <!-- ステップ２ -->
    <div class="step_item">
        <div class="step_title">
            <div class="step_number">
                <p>STEP.<span>２</span></p>
            </div>
            <div class="step_text">
                <h2>専属スタッフに何でも聞いて下さい！</h2>
            </div>
        </div>
        <img src="<?php bloginfo('template_url'); ?>/img/step2.jpg">
        <div class="text">
            <p>Pickでは専門の専属スタッフが全力サポート致します。</p>
            <p>ご希望条件に沿って専属スタッフがヒアリングを行い、数多くのお店の中から貴方に合ったお店をご提案させて頂きます。</p>
            <p class="noblank">「ラウンジとキャバクラの違いって？」</p>
            <p class="noblank">「希望条件が分からない」</p>
            <p>という未経験者の方にも、1から丁寧にご説明させて頂くので、他では聞きにくいという事も何でも聞いて下さい♪</p>
        </div>
    </div>
    <!-- ステップ３ -->
    <div class="step_item">
        <div class="step_title">
            <div class="step_number">
                <p>STEP.<span>３</span></p>
            </div>
            <div class="step_text">
                <h2>お店で面談</h2>
            </div>
        </div>
        <img src="<?php bloginfo('template_url'); ?>/img/step3.jpg">
        <div class="text">
            <p>実際にお店の中を見たり、面談をして雰囲気を確かめる事が出来ます。</p>
            <p>面談をしてみて「他も見てみたい」という事があれば、もちろん他の店舗も見てみる事も可能です♪</p>
            <p class="noblank">面談では女性がお店を見ると同時に、お店が女性を見る場ともなります。</p>
            <p>気に入って頂いたお店でも不採用となってしまう事も少なからずあります。</p>
            <p class="notes noblank">ただ、Pickでは採用確率を上げる事前準備として面談対策もしっかり行っています！</p>
            <p class="notes noblank">未経験者の方でも良い条件で働いて頂けるように全力サポート致しますのでご安心下さい。</p>
        </div>
    </div>
    <!-- ステップ４ -->
    <div class="step_item">
        <div class="step_title">
            <div class="step_number">
                <p>STEP.<span>４</span></p>
            </div>
            <div class="step_text">
                <h2>体験入店</h2>
            </div>
        </div>
        <img src="<?php bloginfo('template_url'); ?>/img/step4.jpg">
        <div class="text">
            <p>面談でご希望の条件があったら、ご希望の日時で体験入店して頂きます。</p>
            <p>体験入店では全額日払いのお店が多いので、その日の内にお給料を貰って帰る事が出来ます。</p>
            <p>また体験入店をした後でも他のお店を見る事も可能ですので、ご希望がある場合は専属スタッフにご相談下さい♪</p>
        </div>
    </div>
    <!-- ステップ５ -->
    <div class="step_item">
        <div class="step_title">
            <div class="step_number">
                <p>STEP.<span>５</span></p>
            </div>
            <div class="step_text">
                <h2>本入店！</h2>
            </div>
        </div>
        <img src="<?php bloginfo('template_url'); ?>/img/step5.jpg">
        <div class="text">
            <p>体験入店をしてみて「やってみたい！」と思えたら、本入店となります。</p>
            <p>本入店する事で時給以外の各種バックであったり、より稼げる環境となります♪</p>
            <p>また入店後もPickでは専属スタッフがサポートを致しますので、万が一トラブルがあった場合やお店の人には少し聞きにくいなという事があれば、いつでもご相談下さい。</p>
        </div>
    </div>
</div>

<!-- ホームに戻る -->
<div class="btn_home">
    <a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
</div>

<?php get_footer(); ?>