<?php get_header(); ?>

<!-- タイトル画像 -->
<div class="page-heading">
    <img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_top.png" alt="">
    <h1>利用女性の声</h1>
    <img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_bottom.png" alt="">
</div>

<div class="voice">
    <h2>Pickを通して、ナイトワークを紹介させて頂いた<br>方々の声をご紹介させて頂きます。</h2>
    <div class="voice_wrapper">
        <div class="voice_item">
            <div class="user">
                <img class="user-image user-image_mb" src="<?php bloginfo('template_url'); ?>/img/voice_icon_user1.png" alt="">
                <div class="user-name">
                    <p>はるかさん(仮)</p>
                    <p>専門学生（20歳）</p>
                </div>
            </div>
            <div class="voice-pc">
                <img class="user-image user-image_pc" src="<?php bloginfo('template_url'); ?>/img/voice_icon_user1.png" alt="">
                <div class="voice_text">
                    <p>学校は忙しいし、短い時間で給料が良いバイトはないかなと探していたところPickのサイトを見つけました。<br>
                        <span>LINEで気軽に分からないことも聞けるし、相談にも乗ってくれる</span>ので安心してお店を探すことが出来ました♪</p>
                    <p>学校もバイトも両立して、効率よく稼ぐことが出来ています。</p>
                </div>
            </div>
        </div>
        <div class="voice_item">
            <div class="user">
                <img class="user-image user-image_mb" src="<?php bloginfo('template_url'); ?>/img/voice_icon_user2.png" alt="">
                <div class="user-name">
                    <p>かんなさん(仮)</p>
                    <p>美容師（23歳）</p>
                </div>
            </div>
            <div class="voice-pc">
                <img class="user-image user-image_pc" src="<?php bloginfo('template_url'); ?>/img/voice_icon_user2.png" alt="">
                <div class="voice_text">
                    <p>お昼やりながらガールズバーでずっと働いてたんですけど、海外で技術学んでみたくなって、もっとお金貯めなきゃって思ったんですよ。</p>
                    <p>でも、ノルマがない環境で働いてきたからラウンジとか難しいのかなって思いながらダメ元で相談してみました。<br>
                        そしたら<span>ラウンジの方がノルマや同伴がないお店が多いとの事で希望以上のお店を紹介して貰えました！</span><br>
                        そんなお店があるなんて自分だけだと分からなかったと思うのであの時<span>ダメ元でもいいから相談してよかった</span>なって本当に感謝しています^^</p>
                </div>
            </div>
        </div>
        <div class="voice_item">
            <div class="user">
                <img class="user-image user-image_mb" src="<?php bloginfo('template_url'); ?>/img/voice_icon_user3.png" alt="">
                <div class="user-name">
                    <p>みさとさん(仮)</p>
                    <p>芸能関係（25歳）</p>
                </div>
            </div>
            <div class="voice-pc">
                <img class="user-image user-image_pc" src="<?php bloginfo('template_url'); ?>/img/voice_icon_user3.png" alt="">
                <div class="voice_text">
                    <p>給料が安定しなくて夜のお仕事もしようと思い、Pickに登録しました。<br>
                        芸能関係をしていることもあり、バレたらまずいのでお店選びは慎重にしたい旨を伝えたら、今のお店をご提案して頂きました。</p>
                    <p>会員制ラウンジなので<span>お客様の業種も把握できている</span>のでバレては<span>まずい席などを避けながら仕事をする</span>ことが出来るので安心して働けています♪<br>
                        こんなわがままな私の希望の条件に合ったお店に出会えると思っていなかったので、相談してみて本当に良かったです^^<br>
                        芸能関係の仕事も順調です♪</p>
                </div>
            </div>
        </div>
        <div class="voice_item">
            <div class="user">
                <img class="user-image user-image_mb" src="<?php bloginfo('template_url'); ?>/img/voice_icon_user4.png" alt="">
                <div class="user-name">
                    <p>あんりさん(仮)</p>
                    <p>フリーター（24歳）</p>
                </div>
            </div>
            <div class="voice-pc">
                <img class="user-image user-image_pc" src="<?php bloginfo('template_url'); ?>/img/voice_icon_user4.png" alt="">
                <div class="voice_text">
                    <p>地方で暮らしてて、上京して働きたいとずっと思っていました。<br>
                        そしたら、友達がPickをお勧めしてくれて相談だけでもしてみようかなと思い、LINEを送りました。</p>
                    <p>東京のことが何もわからない私に親切に細かく教えてくれて、上京するので住む居場所も決めてなかったことまで考えてくれて、<span>寮がついているお店を紹介してもらいました！</span><br>
                        今は無事に上京してきて、恵比寿で働きながら東京生活を楽しんでいます^^<br>
                        地方では出来ない経験のきっかけをくれたPickと教えてくれた友達に感謝しています。</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ホームに戻る -->
<div class="btn_home">
    <a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
</div>

<?php get_footer(); ?>