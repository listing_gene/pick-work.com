<?php get_header(); ?>

<?php
$s = $_GET['s'];
$area = $_GET['area'];
$industry = $_GET['industry'];
$payment = $_GET['payment'];
$condition_details = $_GET['condition_details'];
if ($area) {
	$area = array (
		'taxonomy' => 'area',
		'name' => 'area',
		'terms' => $area,
		'field' => 'slug',
		'operator' => 'AND',
	);
	$area['relation'] = 'AND';
}
if ($industry) {
	$industry = array (
		'taxonomy' => 'industry',
		'terms' => $industry,
		'field' => 'slug',
		'operator' => 'AND',
	);
	$industry['relation'] = 'AND';
}
if ($payment) {
	$payment = array (
		'taxonomy' => 'payment',
		'terms' => $payment,
		'field' => 'slug',
		'operator' => 'AND',
	);
	$payment['relation'] = 'AND';
}
if (isset($condition_details)) {
	$condition_details = array(
		'taxonomy' => 'condition_details',
		'terms' => $condition_details,
		'field' => 'slug',
		'operator' => 'AND',
	);
	$condition_details['relation'] = 'AND';
}
$args = array(
	'post_type' => 'tenpo',
	'paged' => get_query_var('paged'),
	'tax_query' => array(
		'relation' => 'AND', /* 条件完全一致 */
		$area,
		$industry,
		$payment,
		$condition_details
	),
);
query_posts($args);
?>

<!-- パンくずリスト -->
<div class="breadcrumb">
Pick > 検索結果
</div>

		<form method="get" role="search" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
		<div class="wrap_search">
		
		<!-- エリアドロップダウン -->
			<div class="selectbox sbmenu">
					<?php $terms=get_terms('area'); //タームの取得
					if($terms):
					?>
					<select class="form-control" name="area">
						<option value="">全エリア</option>
					<?php foreach($terms as $term):
					$term_slug = $term -> slug;
					$term_title = $term -> name;
					?>
				<option value="<?php echo esc_attr( $term_slug ); ?>" <?php selected($_GET['area'],$term_slug); ?>><?php echo esc_attr( $term_title ); ?></option>
					<?php endforeach; ?>
					</select>
					<?php endif; ?>
		</div>

		<!-- 職種ドロップダウン -->
			<div class="selectbox sbmenu">
					<?php $terms=get_terms('industry'); //タームの取得
					if($terms):
					?>
					<select class="form-control" name="industry">
						<option value="">全業種</option>
					<?php foreach($terms as $term):
					$term_slug = $term -> slug;
					$term_title = $term -> name;
					?>
						<option value="<?php echo esc_attr( $term_slug ); ?>" <?php selected($_GET['industry'],$term_slug); ?>><?php echo esc_attr( $term_title ); ?></option>
					<?php endforeach; ?>
					</select>
					<?php endif; ?>
			</div>

		<!-- 支払い方法ドロップダウン -->
			<div class="selectbox sbmenu">
					<?php $terms=get_terms('payment'); //タームの取得
					if($terms):
					?>
					<select class="form-control" name="payment">
						<option value="">全支払い方法</option>
					<?php foreach($terms as $term):
					$term_slug = $term -> slug;
					$term_title = $term -> name;
					?>
					<option value="<?php echo esc_attr( $term_slug ); ?>" <?php selected($_GET['payment'],$term_slug); ?>><?php echo esc_attr( $term_title ); ?></option>
					<?php endforeach; ?>
					</select>
					<?php endif; ?>
			</div>
		</div>

		<!-- メリット -->
		<div class="wrap_condition">
		<div id="condition_title">
			<p>希望する項目条件をタップして絞り込み</p>
		</div>
		<ul id="condition">
            <?php $taxonomy_name = 'condition_details';
            $taxonomys = get_terms($taxonomy_name, $args);
            if ( !empty($taxonomys) && !is_wp_error($taxonomys)): //配列を受け取ることができ、値がエラーでなかった場合
                foreach ($taxonomys as $taxonomy):
            ?>
			<li class="<?php echo $taxonomy->slug; ?>">
				<input type="checkbox" name="condition_details[]" value="<?php echo $taxonomy->slug; ?>" <?php if(isset($_GET['condition_details']) && in_array($taxonomy->slug,$_GET['condition_details'])): ?>checked="checked"<?php endif; ?>/>
				<label></label>
			</li>
                <?php endforeach; ?>
                <?php endif; ?>
		</ul>
		</div>
    <input type="hidden" name="s" id="s">
		<div id="btn_search">
		<input type="submit" name="search" class="searchsubmit" value="<?php echo esc_attr_x( '検索する', 'submit button' ) ?>" />
		</div>
		</form>

<!-- タイトル画像 -->
<div class="title_common">
    <img src="<?php bloginfo('template_url'); ?>/img/title_top.png" alt="">
    <h2>店舗一覧</h2>
    <img src="<?php bloginfo('template_url'); ?>/img/title_bottom.png" alt="">
</div>

<!-- 件数表示 -->
<div class="posts">
    <p>
        <?php
        if ( have_posts() ) : 
my_result_count();
while ( have_posts() ) :
the_post();
/* do stuff */
endwhile;
else : ?>
<style>
.posts{display: none;} /* 件数が０件だったら非表示 */
</style>
<?php endif; ?></p>
</div>

<div class="shoplist_wrapper">
<?php
    global $wp_query;
    $total_results = $wp_query->found_posts;
	$search_query = get_search_query();
?>
 
 <?php
if(have_posts()):
while(have_posts()): the_post();
?>

<!-- ループ開始 -->
	<!-- スマホ用 -->
	<div class="shoplist">
		<div class="shoplist_top">
		<!-- 店舗名 -->
		<div class="shopname">
			<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
		</div>
		<!-- 所属タグの表示 -->
		<div class="terms">
			<?php $terms = get_the_terms($post->ID,'area');
				foreach( $terms as $term ) {
				echo '<a class="side_area" href="'.get_term_link($term->slug, 'area').'">'.$term->name.'</a>'; } ?>
			<?php $terms = get_the_terms($post->ID,'industry');
				foreach( $terms as $term ) {
				echo '<a class="side_industry" href="'.get_term_link($term->slug, 'industry').'">'.$term->name.'</a>'; } ?>
			</div>
		<!-- 店舗画像 -->
			<a href="<?php the_permalink(); ?>"><img class="shoplist_image_archive" src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
		<p><?php the_field("tenpo-comment"); ?></p>
		</div>
		<!-- 基本情報 -->
		<div class="info">
		<ul class="option op01">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-area' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-time' ) ); ?></span></li>
		</ul>
		<ul class="option op02">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-salay' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-payment' ) ); ?></span></li>
		</ul>
		</div>
		<!-- 所属メリット -->
		<ul id="condition">
            <?php $taxonomy_name = 'condition_details';
            $taxonomys = get_terms($taxonomy_name, $args);
            if ( !empty($taxonomys) && !is_wp_error($taxonomys)):
                foreach ($taxonomys as $taxonomy):
			?>
			<?php if(has_term($taxonomy->slug, 'condition_details')): ?>
			<li class="">
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
				<?php else: ?>
			<li class="">
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
				<?php endif; ?>
				<?php endforeach; ?>
				
                <?php endif; ?>
		</ul>
		<!-- 応募ボタン -->
		<div class="apply">
			<div class="apply_line">
			<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_line_w.png" alt="LINEで応募する">
			</a>
			</div>
			<div class="apply_mail">
			<a class="mail-apply" href="<?php echo home_url('/contact'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_mail_w.png" alt="メールで応募する">
			</a>
			</div>
		</div>
		<!-- 詳細を見る -->
		<div class="more-btn">
		<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る<span>&nbsp;&nbsp;></span></a>
		</div>
	</div>

<!-- PC用 -->
<div class="shoplist_pc">
	<div class="shoplist_top">
		<!-- 店舗名 -->
	<div class="shopname">
		<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
	</div>
	<!-- 所属タグの表示 -->
	<div class="terms">
		<?php $terms = get_the_terms($post->ID,'area');
			foreach( $terms as $term ) {
			echo '<a class="side_area" href="'.get_term_link($term->slug, 'area').'">'.$term->name.'</a>'; } ?>
		<?php $terms = get_the_terms($post->ID,'industry');
			foreach( $terms as $term ) {
			echo '<a class="side_industry" href="'.get_term_link($term->slug, 'industry').'">'.$term->name.'</a>'; } ?>
	</div>
			</div>
<div class="shoplist_left">
	<!-- 店舗画像 -->
	<a class="top_img" href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
	<!-- 応募ボタン -->
	<div class="apply">
		<div class="apply_line">
		<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
			<img src="<?php bloginfo('template_url'); ?>/img/btn_line_w.png" alt="LINEで応募する">
		</a>
		</div>
		<div class="apply_mail">
		<a class="mail-apply" href="<?php echo home_url('/contact'); ?>">
			<img src="<?php bloginfo('template_url'); ?>/img/btn_mail_w.png" alt="メールで応募する">
		</a>
		</div>
	</div>
</div>
<div class="shoplist_right">
	<!-- 店舗説明 -->
	<p><?php the_field("tenpo-comment"); ?></p>
	<!-- 基本情報 -->
	<div class="info">
		<ul class="option op01">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-area' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-time' ) ); ?></span></li>
		</ul>
		<ul class="option op02">
		<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-salay' ) ); ?></span></li>
		<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-payment' ) ); ?></span></li>
		</ul>
	</div>
	<!-- メリット -->
	<ul id="condition">
	<?php $taxonomy_name = 'condition_details';
		$taxonomys = get_terms($taxonomy_name, $args);
		if ( !empty($taxonomys) && !is_wp_error($taxonomys)):
		foreach ($taxonomys as $taxonomy): ?>
		<?php if(has_term($taxonomy->slug, 'condition_details')): ?>
		<li>
			<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
		</li>
		<?php else: ?>
		<li>
			<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
		</li>
		<?php endif; ?>
		<?php endforeach; ?>
		<?php endif; ?>
	</ul>
	<!-- 詳細を見る -->
	<div class="more-btn top-more-btn">
	<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る<span>&nbsp;&nbsp;></span></a>
	</div>
</div>
		</div>
				<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata(); ?>
</div>

<!-- 検索に一致する店舗がない場合 -->
<?php if ($total_results == 0): ?>

	<div class="disagreement">
		<p>条件に一致する店舗は見つかりませんでした。</p>
	</div>

<?php endif; ?>

<!-- ページネーション（０件でなかったら表示する） -->
<?php if (!($total_results == 0)): ?>
<div class="pagination">
 <?php wp_pagenavi(); ?>
</div>
<?php endif; ?>
    
<!-- ホームに戻る -->
<div class="btn_home">
    <a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
</div>

<?php get_footer(); ?>