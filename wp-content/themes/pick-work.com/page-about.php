<?php get_header(); ?>

<!-- タイトル画像 -->
<div class="page-heading">
    <img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_top.png" alt="">
    <h1>ラウンジについて</h1>
    <img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_bottom.png" alt="">
</div>

<div class="lounge">
    <!-- ラウンジイメージ画像 -->
    <div class="lounge_top">
        <img class="lounge_image" src="<?php bloginfo('template_url'); ?>/img/lounge_img.jpg" alt="ラウンジ画像">
    </div>
    <!-- ラウンジとは -->
    <div class="about_lounge">
        <div class="about_title">
            <h2>会員制ラウンジとは</h2>
        </div>
        <div class="about_text">
            <p class="next-line">会員制ラウンジとは、ナイトワークの中でも人気業種となります。</p>
            <p>「女性もお客様」というのが会員制ラウンジのコンセプト！</p>
            <p>キャバクラとは違い、お客様のドリンクを作ったりタバコに火をつける必要がありません。</p>
            <p>基本的に座ってドリンクを飲みながらお話するのがメインです。</p>
            <p>ノルマやペナルティがなく、自由シフト制で自分のライフスタイルに合わせて働くことが出来るのが大きな特徴となっております。</p>
            <p>アフターや営業LINEの強要もなく、気軽に働きやすいことから、お昼のお仕事とWワークで働いている人も多くいらっしゃいます。</p>
            <p class="next-line">また、当日日払いで給料がもらえるという所も魅力の一つとなっております。</p>
            <p>来店するお客様は「一部上場企業の社長・会長」「オーナー」「役員」とハイステータスの人ばかりになりますので、稼ぎながら普段出会えない方々とも繋がれるのも魅力と言えるでしょう♪</p>
        </div>
    </div>
    <!-- LINEバナー -->
    <a class="btn_line" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
        <img src="<?php bloginfo('template_url'); ?>/img/btn_line_lounge.png" alt="面接から入店まで完全フォロー。未経験大歓迎。詳細についてLINEで相談、問い合わせる。">
    </a>
    <!-- 求人情報 -->
    <div class="recruitment">
        <div class="contents_head">
            <img src="<?php bloginfo('template_url'); ?>/img/title_top.png" alt="">
            <h2>求人情報</h2>
            <img src="<?php bloginfo('template_url'); ?>/img/title_bottom.png" alt="">
        </div>
        <div class="recruitment_list">
            <ul>
                <li>業種</li>
                <li>会員制ラウンジ</li>
            </ul>
            <ul>
                <li>主なエリア</li>
                <li>
                    <p>恵比寿、西麻布、六本木、銀座</p>
                    <p>その他、都内を中心に関東圏一帯を完全網羅！</p>
                </li>
            </ul>
            <ul>
                <li>服装</li>
                <li>
                    <p>カジュアルすぎない私服がドレスコードとなってます♪<p>
                            <p>学校・会社帰りにそのままの私服で出勤可能！</p>
                </li>
            </ul>
            <ul>
                <li>営業時間</li>
                <li>
                    <p>20:00〜25:00</p>
                    <p class="notes">※店舗によって異なります。</p>
                </li>
            </ul>
            <ul>
                <li>定休日</li>
                <li>
                    <p>日曜・祝日</p>
                    <p>＜自己申告制シフト＞</p>
                    <p>短期もOK！ライフスタイルに合わせて自由に出勤できます♪</p>
                    <p class="notes">※店舗によって異なります。</p>
                </li>
            </ul>
            <ul>
                <li>時給</li>
                <li>時給4,000円〜10,000円以上</li>
            </ul>
            <ul>
                <li>バック</li>
                <li>
                    <p>・ドリンクバック</p>
                    <p>・ボトルバック</p>
                    <p>・リクエスト</p>
                    <p>・リザーブ</p>
                    <p>・アテンド等</p>
                    <p>♢各種バック完備！</p>
                </li>
            </ul>
            <ul>
                <li>備考</li>
                <li>
                    <p>♢未経験大歓迎！</p>
                    <p>♢学生さん、OLさん、Wワークの方も大歓迎！</p>
                    <p>♢学校帰りにそのままの私服で出勤可能！</p>
                    <p>♢送り有り</p>
                    <p>♢ノルマ・ペナルティ一切なし</p>
                    <p>♢面接から入店、入店後も完全フォロー</p>
                    <p class="notes">※18歳以上（高校生不可）</p>
                </li>
            </ul>
        </div>
    </div>
    <!-- 安心の理由 -->
    <div class="reason">
        <div class="contents_head">
            <img src="<?php bloginfo('template_url'); ?>/img/title_top.png" alt="">
            <h2>安心の理由</h2>
            <img src="<?php bloginfo('template_url'); ?>/img/title_bottom.png" alt="">
        </div>
        <div class="reason_list">
            <!-- ポイント１ -->
            <div class="reason_item">
                <div class="point_number">
                    <p><i class="fas fa-check"></i>POINT.1</p>
                </div>
                <div class="point_text">
                    <p>未経験、初心者でも</p>
                    <p>気軽に働ける</p>
                </div>
            </div>
            <!-- ポイント２ -->
            <div class="reason_item">
                <div class="point_number">
                    <p><i class="fas fa-check"></i>POINT.2</p>
                </div>
                <div class="point_text">
                    <p>延長営業</p>
                    <p>一切なし</p>
                </div>
            </div>
            <!-- ポイント３ -->
            <div class="reason_item">
                <div class="point_number">
                    <p><i class="fas fa-check"></i>POINT.3</p>
                </div>
                <div class="point_text">
                    <p>有名人・富裕層など</p>
                    <p>客層が上品</p>
                </div>
            </div>
            <!-- ポイント４ -->
            <div class="reason_item">
                <div class="point_number">
                    <p><i class="fas fa-check"></i>POINT.4</p>
                </div>
                <div class="point_text">
                    <p>ノルマ・ペナルティが</p>
                    <p>一切なし</p>
                </div>
            </div>
        </div>
    </div>
    <!-- テキスト -->
    <div class="comment">
        <p class="next-line">会員制ラウンジで働きたい方には、希望に合う会員制ラウンジのご紹介等、ピックが全力でサポート、マネジメント致します。</p>
        <p>「どうやってお店を選んでいいか分からない」</p>
        <p>「面接の対策を教えて欲しい」</p>
        <p>など入店までのことはもちろん、入店後のフォローまで、24時間体制で対応させて頂きます。</p>
    </div>
    <!-- LINEバナー -->
    <a class="btn_line" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
        <img src="<?php bloginfo('template_url'); ?>/img/btn_line_lounge.png" alt="面接から入店まで完全フォロー。未経験大歓迎。詳細についてLINEで相談、問い合わせる。">
    </a>
    <!-- 応募の流れリンク -->
    <div class="img_btn step_link"><a href="<?php echo home_url('/step'); ?>">
            <img src="<?php bloginfo('template_url'); ?>/img/btn_step.png" alt="応募の流れ">
        </a>
    </div>
    <!-- ホームに戻る -->
    <div class="btn_home">
        <a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
    </div>
</div>

<?php get_footer(); ?>