<footer id="footer">
	<div class="footer_contents">
	<i class="fas fa-angle-right"></i>
		<a href="<?php echo home_url('/company'); ?>">運営会社</a>
		<i class="fas fa-angle-right"></i>
		<a href="<?php echo home_url('/privacy'); ?>">プライバシーポリシー</a>
	</div>
	<p class="top-link"><a href="#">▲ TOP</a></p>
	<div class="footer_text">
		<p>サイト上の表記について</p>
		<p>※契約先は当社では無く当サイト掲載店になります。</p>
		<p>※当サイト記載の労働条件について</p>
		<p>内容・時期・掲載店・その他事情により、<br>サイトに記載されている条件では対応できない場合がございます。</p>
	</div>
	<p class="copy">&copy; 2019 Pick</p>
</footer>
</div>

<!-- ピックアップ店舗 -->
<script type="text/javascript">
$(function() {
	$('.slider_tenpo').slick({
		dots: true, /* 画像下の点を表示 */
		speed: 700,
		autoplay: false, /* 自動オフ */
		autoplaySpeed: 7000,
	});
});
</script>
<!-- ヘッダースライド画像 -->
<script type="text/javascript">
var windowWidth = $(window).width();
var windowSm = 768;
if (windowWidth <= windowSm) {
$(function() {
	$('.slider-header').slick({
		dots: false, /* 画像下の点を表示 */
		speed: 1000,
		infinite: true, /* スライドのループ有効化 */
		autoplay: true, /* 自動オン */
		autoplaySpeed: 3000,
		fade: true,
	});
});
} else {
$(function() {
	$('.slider-header').slick({
		dots: false, /* 画像下の点を表示 */
		accessibility: true,
		speed: 1000,
		infinite: true, /* スライドのループ有効化 */
		autoplay: true, /* 自動オン */
		autoplaySpeed: 5000,
		centerMode: true,
		centerPadding: '25%',
		centeredSlides: true,
	});
});
}
</script>
<!-- ハンバーガーメニュー -->
<script>
	$(function() {
		$('.navbar_toggle').on('click', function() {
			$(this).toggleClass('open');
			$('.menu').toggleClass('open');
		});
	});
</script>
</body>
</html>
<?php wp_footer(); ?>