<?php get_header(); ?>

	<!-- パンくずリスト -->
	<div class="breadcrumb">
  <?php if(function_exists('bcn_display'))
  {
   bcn_display();
  }?>
	</div>

	<?php if(have_posts()): while(have_posts()):the_post(); ?>
    
	<!-- 記事 -->
	<div class="column_post">
		<!-- コラムタイトル -->
		<h1>
		<?php the_title(); ?>
		</h1>
		<!-- コラム本文 -->
		<div class="column_text">
		<?php the_content(); ?>
		</div>
	</div>
  	<!-- aimaリンクバナー -->
  	<div class="aima-banner">
	<p>▼全国展開！業界No1&thinsp;ギャラ飲みならaima</p>
	  <a href="https://aima-match.com/cast_pbqsrf/"><img src="<?php bloginfo('template_url'); ?>/img/aima_bnr.png" alt="居酒屋・バー・カラオケなど行き慣れた店舗で合流可能。ギャラ飲みならaima。LINE友達追加で簡単登録"></a>
	  <small>※周囲に知られたり勝手に投稿されたりすることは一切ありません</small>
	  </div>

<?php endwhile; endif; ?>

	<!-- コラム一覧に戻る -->
	<div class="btn_home">
		<a href="<?php echo get_post_type_archive_link( 'column' ); ?>" class="btn_border">コラム一覧に戻る</a>
	</div>

<?php get_footer(); ?>