<?php get_header(); ?>

<!-- タイトル画像 -->
<div class="page-heading">
	<img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_top.png" alt="">
	<h1>なぜ、Pickが選ばれているのか</h1>
	<img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_bottom.png" alt="">
</div>

<div class="elected">
	<div class="elected_item">
		<div class="elected_title">
			<h2>厚生労働省の公認サイト</h2>
		</div>
		<img class="elected_img" src="<?php bloginfo('template_url'); ?>/img/logo_mhlw.png" alt="">
		<div class="elected_text">
			<p>Pickは厚生労働省公認のナイトワーク紹介会社です。</p>
			<p>紹介先の店舗も高級店、有名店、大手グループ店などご希望条件に沿って女性が安心して働ける店舗のみの紹介をさせて頂いてます。</p>
			<p>もちろん、店舗の紹介するにあたって一切料金が掛かることなく完全無料で行わせて頂いているので「興味がある」「気になる」だけでも気軽にLINE相談して頂けます。</p>
		</div>
    	<!-- LINEバナー -->
    	<a class="btn_line" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
        	<img class="banner_line" src="<?php bloginfo('template_url'); ?>/img/btn_line_lounge.png" alt="面接から入店まで完全フォロー。未経験大歓迎。詳細についてLINEで相談、問い合わせる。">
    	</a>
	</div>
	<div class="elected_item">
		<div class="elected_title">
			<h2>エリア毎の専門スタッフが専属担当</h2>
		</div>
		<img class="elected_img" src="<?php bloginfo('template_url'); ?>/img/elected_icon_hand.png" alt="">
		<div class="elected_text">
			<p>各エリアに特化した専門スタッフが完全サポートさせて頂きます。</p>
			<p>沢山の提携店舗がある中でも、ご希望条件にマッチした店舗をご提案出来るのはエリアに特化した専門スタッフを配置しているPickならではの強みとなっております。</p>
		</div>
	</div>
	<div class="elected_item">
		<div class="elected_title">
			<h2>時給UP交渉（500〜3,000円）</h2>
		</div>
		<img class="elected_img" src="<?php bloginfo('template_url'); ?>/img/elected_icon_up.png" alt="">
		<div class="elected_text">
			<p>働くなら出来るだけ高い時給で働きたいと思っていても、女性自身でお店と時給交渉をするのは難しい事だと思います。</p>
			<p>そこでPickは女性に代わって時給や待遇の交渉を行い、少しでも良い条件で働いて頂けるようにサポートさせて頂きます。</p>
		</div>
		<div class="past-cases">
			<div class="past-cases_title">
				<h3>過去事例</h3>
			</div>
			<div class="past-cases_wrapper">
			<div class="past-cases_item">
				<div class="left">
				<img class="elected_img" src="<?php bloginfo('template_url'); ?>/img/elected_icon_woman1.png" alt="">
				</div>
				<div class="right">
					<p>MIHOさん</p>
					<p>西麻布高級ラウンジ</p>
					<p>時給3,500円&nbsp;<i class="fas fa-arrow-right"></i>&nbsp;<span>6,000円</span></p>
					<p>（<span>2,500円UP</span>）</p>
				</div>
			</div>
			<div class="past-cases_item">
				<div class="left">
				<img class="elected_img" src="<?php bloginfo('template_url'); ?>/img/elected_icon_woman2.png" alt="">
				</div>
				<div class="right">
					<p>AYAさん</p>
					<p>六本木高級キャバクラ</p>
					<p>時給4,000円&nbsp;<i class="fas fa-arrow-right"></i>&nbsp;<span>7,000円</span></p>
					<p>（<span>3,000円UP</span>）</p>
				</div>
			</div>
			<div class="past-cases_item">
				<div class="left">
				<img class="elected_img" src="<?php bloginfo('template_url'); ?>/img/elected_icon_woman3.png" alt="">
				</div>
				<div class="right">
					<p>KAORIさん</p>
					<p>恵比寿ラウンジ</p>
					<p>時給5,000円&nbsp;<i class="fas fa-arrow-right"></i>&nbsp;<span>6,500円</span></p>
					<p>（<span>1,500円UP</span>）</p>
				</div>
			</div>
			</div>
		</div>
	</div>
	<div class="elected_item">
		<div class="elected_title">
			<h2>面接対策、同行</h2>
		</div>
		<img class="elected_img" src="<?php bloginfo('template_url'); ?>/img/elected_icon_measures.png" alt="厚生労働省">
		<div class="elected_text">
			<p>未経験者、初心者の方は面接経験もなく不安があるかと思いますが、Pickでは採用確率を上げる事前準備として面談対策もしっかり行っています！</p>
			<p>未経験者の方でも良い条件で働いて頂けるように全力サポート致しますのでご安心下さい。</p>
		</div>
	</div>
	<div class="elected_item">
		<div class="elected_title">
			<h2>即日面接、体入可能</h2>
		</div>
		<img class="elected_img" src="<?php bloginfo('template_url'); ?>/img/elected_icon_24.png" alt="厚生労働省">
		<div class="elected_text">
			<p>ご連絡頂いたその日のうちに面接や体入を組む事も可能です。</p>
			<p>ナイトワークご紹介希望の多くの方が早く決めたいという事もあるので、Pickでは365日24時間スピード対応を心掛けています。</p>
		</div>
	</div>
	<p class="last-text">Pick独自の強みをもとに<br>あなたにピッタリの店舗をご提案致します。</p>
    <!-- LINEバナー -->
    <a class="btn_line" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
        <img class="banner_line" src="<?php bloginfo('template_url'); ?>/img/btn_line_lounge.png" alt="LINEで問い合わせる">
    </a>
</div>


<!-- ホームに戻る -->
<div class="btn_home">
	<a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
</div>
    
<?php get_footer(); ?>