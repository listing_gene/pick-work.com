<?php get_header(); ?>

<!-- タイトル画像 -->
<div class="page-heading">
	<img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_top.png" alt="">
	<h1>運営会社</h1>
	<img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_bottom.png" alt="">
</div>

<!-- 会社情報 -->
<div class="company">
	<div class="corp">
		<table>
			<tbody>
				<tr>
					<th>会社名</th>
					<td>pick運営事務局</td>
				</tr>
				<tr>
					<th>Mail</th>
					<td><a href="mailto:info@pick-work.com">info@pick-work.com</a></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>


<!-- ホームに戻る -->
<div class="btn_home"><a class="btn_border" href="http://pick-work.com">ホームに戻る</a></div>

<?php get_footer(); ?>