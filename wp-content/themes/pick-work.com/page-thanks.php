<!DOCTYPE html>
<html>

<head>
	<title><?php wp_title(); ?></title>
	<meta charset="UTF-8">
	<meta name=”keywords” content=”会員制ラウンジ,キャバクラ,バイト,求人,高額バイト”>
	<meta name="viewport" content="width=device-width,initial-scale=1,viewport-fit=cover">
	<link rel="shortcut icon" href="https://pick-work.com/wp-content/uploads/2019/07/favicon.png">
	<link href="https://fonts.googleapis.com/css?family=Sawarabi+Mincho&display=swap" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/reset.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css">
	<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- slick -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/slick/slick.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/slick/slick-theme.css" media="screen" />
	<script src="<?php echo get_template_directory_uri(); ?>/slick/slick.min.js"></script>
	<?php wp_head(); ?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141688512-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-141688512-1');
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136387764-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-136387764-2');
	</script>

	<!-- Global site tag (gtag.js) - Google Ads: 742977722 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-742977722"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'AW-742977722');
	</script>

	<!-- Event snippet for pick_mail_CV conversion page -->
	<script>
		gtag('event', 'conversion', {
			'send_to': 'AW-742977722/dTl4CNORzJ8BELrho-IC'
		});
	</script>
    <!-- User Heat Tag -->
    <script type="text/javascript">
        (function(add, cla){window['UserHeatTag']=cla;window[cla]=window[cla]||function(){(window[cla].q=window[cla].q||[]).push(arguments)},window[cla].l=1*new Date();var ul=document.createElement('script');var tag = document.getElementsByTagName('script')[0];ul.async=1;ul.src=add;tag.parentNode.insertBefore(ul,tag);})('//uh.nakanohito.jp/uhj2/uh.js', '_uhtracker');_uhtracker({id:'uhxZn9jIge'});
    </script>
    <!-- End User Heat Tag -->
</head>

<body>
<script type="text/javascript">
(function(s,m,n,l,o,g,i,c,a,d){c=(s[o]||(s[o]={}))[g]||(s[o][g]={});if(c[i])return;c[i]=function(){(c[i+"_queue"]||(c[i+"_queue"]=[])).push(arguments)};a=m.createElement(n);a.charset="utf-8";a.async=true;a.src=l;d=m.getElementsByTagName(n)[0];d.parentNode.insertBefore(a,d)})(window,document,"script","https://cd.ladsp.com/script/pixel2.js","Smn","Logicad","pixel");
Smn.Logicad.pixel({
"smnAdvertiserId":"00010421"
});
</script>
	<!-- ヘッダー -->
	<nav class="header_nav">
		<div class="drawer">
			<div class="navbar_toggle">
				<span class="navbar_toggle_icon"></span>
				<span class="navbar_toggle_icon"></span>
				<span class="navbar_toggle_icon"></span>
			</div>
			<div class="logo">
				<a class="logo_mb" href="<?php echo home_url(); ?>"><h1><img src="<?php bloginfo('template_url'); ?>/img/logo.svg" alt="高級ラウンジ・会員制ラウンジ求人サイト。ピック。業界初のマッチングサービス"></h1></a>
			</div>
			<!-- PC用ヘッダーリスト -->
			<div class="head_list">
				<ul>
					<li><a href="<?php echo home_url('/about'); ?>">ラウンジについて</a></li>
					<li><a href="<?php echo get_post_type_archive_link('tenpo'); ?>">店舗一覧</a></li>
					<li><a href="<?php echo get_post_type_archive_link('column'); ?>">コラム一覧</a></li>
					<li><a href="<?php echo get_post_type_archive_link('info'); ?>">新着情報一覧</a></li>
					<li><a href="<?php echo home_url('/step'); ?>">応募の流れ</a></li>
					<li><a href="<?php echo home_url('/contact'); ?>">お問い合わせ</a></li>
				</ul>
			</div>
			<a class="head_line" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img src="<?php bloginfo('template_url'); ?>/img/btn_line_head.png" alt="lineで相談v"></a>
			<div class="icon">
				<a href="<?php echo home_url('/contact'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/btn_mail.png" alt="mail"></a>
				<a onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')"><img src="<?php bloginfo('template_url'); ?>/img/btn_line.png" alt="line"></a>
			</div>
		</div>
		<img class="deco" src="<?php bloginfo('template_url'); ?>/img/gold_line.png" alt="deco">

		<!-- ハンバーガーメニュー -->
		<div class="menu">
			<ul>
				<li><a href="<?php echo home_url(); ?>">Home</a></li>
				<li><a href="<?php echo home_url('/elected'); ?>">なぜ、Pickが選ばれているのか</a></li>
				<li><a href="<?php echo home_url('/step'); ?>">応募の流れ</a></li>
				<li><a href="<?php echo home_url('/user-voice'); ?>">利用女性の声</a></li>
				<li><a href="<?php echo home_url('/about'); ?>">ラウンジについて</a></li>
				<li><a href="<?php echo get_post_type_archive_link('tenpo'); ?>">店舗一覧</a></li>
				<li><a href="<?php echo get_post_type_archive_link('column'); ?>">コラム一覧</a></li>
				<li><a href="<?php echo get_post_type_archive_link('info'); ?>">新着情報一覧</a></li>
				<li style="margin-bottom:0; border-bottom: none;">
					<!-- SNS -->
					<div class="social">
						<p id="menu">公式SNSをチェック</p>
						<a href="https://www.instagram.com/pick_media"><img src="<?php bloginfo('template_url'); ?>/img/icon_menu_insta.png" alt="Instagram"></a>
						<a href="https://twitter.com/pick_official1"><img src="<?php bloginfo('template_url'); ?>/img/icon_menu_twitter.png" alt="twitter"></a>
					</div>
				</li>
				<div class="menu_deco">
					<img src="<?php bloginfo('template_url'); ?>/img/gold_line.png" alt="deco">
				</div>
			</ul>
		</div>
	</nav>

	<!-- 本文 -->
	<div class="thanx">
		<h1>
			応募完了
		</h1>
		<p>Pickへのご応募・お問い合わせありがとうございます。</p>
		<p>　</p>
		<p>お問い合わせ内容を確認させていただき、</p>
		<p>後ほど担当者よりご回答させていただきます。</p>
		<p>　</p>
		<p>恐れ入りますが、今しばらくお待ちいただけますよう、よろしくお願い申し上げます。</p>
	</div>

	<!-- ホームに戻る -->
	<div class="btn_home">
		<a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
	</div>

	<?php get_footer(); ?>