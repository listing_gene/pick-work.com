<?php get_header(); ?>

<!-- テキスト -->
<h1 style="font-weight: bold;font-size:1.4em">お探しのページは見つかりませんでした</h1>

	このページは一時的にアクセスできない状況にあるか、<br>
	掲載が終了し、URLが変更・削除された可能性がございます。<br>
    
<!-- ホームに戻る -->
<div class="btn_home">
	<a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
</div>

<?php get_footer(); ?>