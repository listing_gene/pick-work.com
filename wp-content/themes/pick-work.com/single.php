<?php if ( in_category('tenpo') ) {
    include(TEMPLATEPATH . '/single/tenpo.php');
} else if ( in_category('info') ) {
    include(TEMPLATEPATH . '/single/info.php');
} else if ( in_category('column') ) {
    include(TEMPLATEPATH . '/single/column.php');
} else {
    include(TEMPLATEPATH . '/single/base.php');
}
?>