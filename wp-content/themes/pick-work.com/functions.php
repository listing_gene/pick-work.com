<?php

function self_made_post_type(){
    register_post_type(
        'tenpo',
        array(
            'label' => '店舗一覧', //表示名
            'public'        => true, //公開状態
            'exclude_from_search' => false, //検索対象に含めるか
            'show_ui' => true, //管理画面に表示するか
            'show_in_menu' => true, //管理画面のメニューに表示するか
            'menu_position' => 5, //管理メニューの表示位置を指定
            'hierarchical' => true, //階層構造を持たせるか
            'has_archive'   => true, //この投稿タイプのアーカイブを作成するか
            'supports' => array(
                'title',
                'editor',
                'comments',
                'excerpt',
                'thumbnail',
                'custom-fields',
                'post-formats',
                'page-attributes',
                'trackbacks',
                'revisions',
                'author'
            ), //編集画面で使用するフィールド
        )
    );
    register_post_type(
        'info',
        array(
            'label' => '新着情報', //表示名
            'public'        => true, //公開状態
            'exclude_from_search' => false, //検索対象に含めるか
            'show_ui' => true, //管理画面に表示するか
            'show_in_menu' => true, //管理画面のメニューに表示するか
            'menu_position' => 5, //管理メニューの表示位置を指定
            'hierarchical' => true, //階層構造を持たせるか
            'has_archive'   => true, //この投稿タイプのアーカイブを作成するか
            'supports' => array(
                'title',
                'editor',
                'comments',
                'excerpt',
                'thumbnail',
                'custom-fields',
                'post-formats',
                'page-attributes',
                'trackbacks',
                'revisions',
                'author'
            ), //編集画面で使用するフィールド
        )
    );
    register_post_type(
        'column',
        array(
            'label' => 'コラム', //表示名
            'public'        => true, //公開状態
            'exclude_from_search' => false, //検索対象に含めるか
            'show_ui' => true, //管理画面に表示するか
            'show_in_menu' => true, //管理画面のメニューに表示するか
            'menu_position' => 5, //管理メニューの表示位置を指定
            'hierarchical' => true, //階層構造を持たせるか
            'has_archive'   => true, //この投稿タイプのアーカイブを作成するか
            'supports' => array(
                'title',
                'editor',
                'comments',
                'excerpt',
                'thumbnail',
                'custom-fields',
                'post-formats',
                'page-attributes',
                'trackbacks',
                'revisions',
                'author'
            ), //編集画面で使用するフィールド
        )
    );
}
add_action('init', 'self_made_post_type', 1);
?>
<?php
function self_made_taxonomies()
{
    // 自作カテゴリーを作成
    register_taxonomy(
        'area',
        array('tenpo'),
        array(
            'label'            => 'エリア', //表示名
            'show_ui'           => true, //管理画面に表示するか
            'show_admin_column' => true, //管理画面の一覧に表示するか
            'show_in_nav_menus' => true, //カスタムメニューの作成画面で表示するか
            'hierarchical'      => true, //階層構造を持たせるか（持たせるとカテゴリー扱い）
        )
    );
    register_taxonomy(
        'industry',
        array('tenpo'),
        array(
            'label'            => '業種', //表示名
            'show_ui'           => true, //管理画面に表示するか
            'show_admin_column' => true, //管理画面の一覧に表示するか
            'show_in_nav_menus' => true, //カスタムメニューの作成画面で表示するか
            'hierarchical'      => true, //階層構造を持たせるか（持たせるとカテゴリー扱い）
        )
    );
    register_taxonomy(
        'payment',
        array('tenpo'),
        array(
            'label'            => '支払い方法', //表示名
            'show_ui'           => true, //管理画面に表示するか
            'show_admin_column' => true, //管理画面の一覧に表示するか
            'show_in_nav_menus' => true, //カスタムメニューの作成画面で表示するか
            'hierarchical'      => true, //階層構造を持たせるか（持たせるとカテゴリー扱い）
        )
    );
    register_taxonomy(
        'condition_details',
        array('tenpo'),
        array(
            'label'            => 'メリット', //表示名
            'show_ui'           => true, //管理画面に表示するか
            'show_admin_column' => true, //管理画面の一覧に表示するか
            'show_in_nav_menus' => true, //カスタムメニューの作成画面で表示するか
            'hierarchical'      => true, //階層構造を持たせるか（持たせるとカテゴリー扱い）
        )
    );
    // 自作タグを作成
    register_taxonomy(
        'tenpo_tag',
        array('tenpo'),
        array(
            'labels'       => 'タグ', //表示名
            'show_ui'      => true, //管理画面に表示するか
            'show_in_nav_menus' => true,
            'hierarchical' => false, //階層構造を持たせるか（持たせないとタグ扱い）
        )
    );
}
add_action('init', 'self_made_taxonomies', 0);
?>
<?php
add_action('wp_footer', 'add_thanks_page');
function add_thanks_page()
{
    echo <<< EOD
<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
  location = 'https://pick-work.com/thanks/'; /* 遷移先のURL */
}, false );
</script>
EOD;
}
?>
<?php
add_action('pre_get_posts', 'foo_modify_main_queries');
function foo_modify_main_queries($query)
{
    if (!is_admin() && $query->is_main_query()) { // 管理画面以外かつメインクエリーを対象とする
        if ($query->is_home()) {
            $query->set('post_type', array('info', 'tenpo', 'column')); // 投稿とカスタム投稿（blog）を含める
        }
    }
}
?>
<?php
function my_result_count() {
  global $wp_query;

  $paged = get_query_var( 'paged' ) - 1;
  $ppp   = get_query_var( 'posts_per_page' );
  $count = $total = $wp_query->post_count;
  $from  = 0;
  if ( 0 < $ppp ) {
    $total = $wp_query->found_posts;
    if ( 0 < $paged )
      $from  = $paged * $ppp;
  }
  printf(
    '<p>全%1$s件中 %2$s%3$s件目を表示</p>',
    $total,
    ( 1 < $count ? ($from + 1 . '〜') : '' ),
    ($from + $count )
  );
}
?>
<?php
function new_excerpt_mblength($length) {
    return 22; //抜粋する文字数
}
add_filter('excerpt_mblength', 'new_excerpt_mblength');
?>
<?php

add_editor_style('style.css');

//////////////////////////////////////////////////
//ビジュアルエディタ項目カスタマイズ
//////////////////////////////////////////////////
function custom_editor_settings( $initArray ){
	$initArray['block_formats'] = "段落=p; 見出し2=h2; 見出し3=h3; 見出し4=h4; 見出し5=h5; 整形済みテキスト=pre;";
	return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );


function custom_tiny_mce_style_formats( $settings ) {
  $style_formats = array(
    array(
        'title' => 'BOX：線枠',
        'block' => 'div',
        'classes' => 'borderBox',
        'wrapper' => true,
    ),
    array(
        'title' => 'BOX：二重線',
        'block' => 'div',
        'classes' => 'border2Box',
        'wrapper' => true,
    ),
    array(
        'title' => 'BOX：背景',
        'block' => 'div',
        'classes' => 'bgBox',
        'wrapper' => true,
    ),
    array(
        'title' => 'BOX：ペーパー',
        'block' => 'div',
        'classes' => 'paperBox',
        'wrapper' => true,
    ),
    array(
        'title' => 'BOX：太文字',
        'block' => 'div',
        'classes' => 'boldBox',
        'wrapper' => true,
    ),
    array(
        'title' => 'BOX：括弧',
        'block' => 'div',
        'classes' => 'bracketsBox',
        'wrapper' => true,
    ),
    array(
        'title' => 'BOX：はてな',
        'block' => 'div',
        'classes' => 'questionBox',
        'wrapper' => true,
    ),
    array(
        'title' => 'BOX：びっくり',
        'block' => 'div',
        'classes' => 'exclamationBox',
        'wrapper' => true,
    ),
    array(
        'title' => 'BOX：ポイント',
        'block' => 'div',
        'classes' => 'pointBox',
        'wrapper' => true,
    ),
	array(
      'title' => 'マーカー：イエロー',
      'inline' => 'span',
      'classes' => 'markerYellow',
      'wrapper' => true,
    ),
	array(
      'title' => 'マーカー：ピンク',
      'inline' => 'span',
      'classes' => 'markerPink',
      'wrapper' => true,
    ),
	array(
      'title' => 'マーカー：ブルー',
      'inline' => 'span',
      'classes' => 'markerBlue',
      'wrapper' => true,
    ),
	array(
      'title' => 'small',
      'inline' => 'span',
      'classes' => 'asterisk',
      'wrapper' => true,
    ),

  );
  $settings[ 'style_formats' ] = json_encode( $style_formats );
  return $settings;
}
add_filter( 'tiny_mce_before_init', 'custom_tiny_mce_style_formats' );


function add_original_styles_button( $buttons ) {
  array_splice( $buttons, 1, 0, 'styleselect' );
  return $buttons;
}
add_filter( 'mce_buttons', 'add_original_styles_button' );

//////////////////////////////////////////////////
//投稿エディタにクイックタグボタン追加
//////////////////////////////////////////////////
if (!function_exists( 'add_quicktags_to_text_editor' ) ) {
	function add_quicktags_to_text_editor() {
		//スクリプトキューにquicktagsが保存されているかチェック
		if (wp_script_is('quicktags')){?>
		<script>
			QTags.addButton('qt-p','p','<p>','</p>');
			QTags.addButton('qt-h2','h2','<h2>','</h2>');
			QTags.addButton('qt-h3','h3','<h3>','</h3>');
			QTags.addButton('qt-h4','h4','<h4>','</h4>');
			QTags.addButton('qt-h5','h5','<h5>','</h5>');
			QTags.addButton('qt-hr','hr','<hr>');
			QTags.addButton('qt-br','br','<br>');
			QTags.addButton('qt-pre','pre','<pre>','</pre>');
			QTags.addButton('qt-asterisk','small','<span class="asterisk">','</span>');
			QTags.addButton('qt-borderBox','BOX：枠線','<div class="borderBox">','</div>');
			QTags.addButton('qt-border2Box','BOX：二重線','<div class="border2Box">','</div>');
			QTags.addButton('qt-bgBox','BOX：背景','<div class="bgBox">','</div>');
			QTags.addButton('qt-paperBox','BOX：ペーパー','<div class="paperBox">','</div>');
			QTags.addButton('qt-boldBox','BOX：太文字','<div class="boldBox">','</div>');
			QTags.addButton('qt-bracketsBox','BOX：括弧','<div class="bracketsBox">','</div>');
			QTags.addButton('qt-questionBox','BOX：はてな','<div class="questionBox">','</div>');
			QTags.addButton('qt-exclamationBox','BOX：びっくり','<div class="exclamationBox">','</div>');
			QTags.addButton('qt-pointBox','BOX：ポイント','<div class="pointBox">','</div>');

			QTags.addButton('qt-markerYellow','マーカー：イエロー','<span class="markerYellow">','</span>');
			QTags.addButton('qt-markerPink','マーカー：ピンク','<span class="markerPink">','</span>');
			QTags.addButton('qt-markerBlue','マーカー：ブルー','<span class="markerBlue">','</span>');

			</script>
		<?php
        }
	}
}
add_action( 'admin_print_footer_scripts', 'add_quicktags_to_text_editor' );
?>
<?php
function get_the_custom_excerpt($content, $length) {
  $length = ($length ? $length : 70);//デフォルトの長さを指定する
  $content =  preg_replace('/<!--more-->.+/is',"",$content); //moreタグ以降削除
  $content =  strip_shortcodes($content);//ショートコード削除
  $content =  strip_tags($content);//タグの除去
  $content =  str_replace("&nbsp;","",$content);//特殊文字の削除（スペース）
  $content =  mb_substr($content,0,$length);//文字列を指定した長さで切り取る
  return $content;
}