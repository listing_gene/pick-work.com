<?php get_header(); ?>

<!-- パンくずリスト -->
<div class="breadcrumb">
	<?php if (function_exists('bcn_display')) {
		bcn_display();
	} ?>
</div>

<!-- タイトル画像 -->
<div class="page-heading page-heading_info">
	<img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_top.png" alt="">
	<h1>新着情報一覧</h1>
	<img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_bottom.png" alt="">
</div>

<div class="arch_info">
	<?php
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$args = array(
		'post_type' => 'info', //カスタム投稿名
		'posts_per_page' => 5, //表示件数
		'paged' => $paged,
	); ?>
	<?php $my_query = new WP_Query($args); ?>
	<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>

		<!-- ループ開始 -->
		<div class="new_prev">
			<!-- アイキャッチ画像 -->
			<div class="new_img">
				<a href="<?php the_permalink(); ?>"><img src="<?php the_field("info-img"); ?>" alt="<?php the_title(); ?>"></a>
			</div>

			<!-- スマホ用（日付、タイトル、本文抜粋） -->
			<div class="new_txt">
				<!-- 投稿日付 -->
				<a class="top_date" href="<?php the_permalink(); ?>"><?php the_time('Y.m.d'); ?></a>
				<!-- タイトル -->
				<a class="top_title" href="<?php the_permalink(); ?>">
					<?php
					if (mb_strlen($post->post_title) > 21) {
						$title = mb_substr($post->post_title, 0, 21);
						echo $title . '...';
					} else {
						echo $post->post_title;
					} ?>
				</a>
				<!-- 本文抜粋 -->
				<a class="top_text" href="<?php the_permalink(); ?>"><?php echo mb_substr(get_the_excerpt(), 0, 26) . '...'; ?></a>
			</div>

			<!-- PC用（日付、タイトル、本文抜粋） -->
			<div class="new_txt_pc">
				<!-- 投稿日付 -->
				<a class="top_date" href="<?php the_permalink(); ?>"><?php the_time('Y.m.d'); ?></a>
				<!-- タイトル -->
				<a class="top_title" href="<?php the_permalink(); ?>">
					<?php
					if (mb_strlen($post->post_title) > 35) {
						$title = mb_substr($post->post_title, 0, 35);
						echo $title . '...';
					} else {
						echo $post->post_title;
					} ?>
				</a>
				<!-- 本文抜粋 -->
				<a class="top_text" href="<?php the_permalink(); ?>"><?php echo get_the_custom_excerpt(get_the_content(), 128) . '...' ?></a>
			</div>
		</div>
		<!-- ループ終了 -->

	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</div>

<!-- ページ送り -->
<div class="pagination">
	<?php
	if (function_exists('wp_pagenavi')) {
		wp_pagenavi(array('query' => $my_query));
	} ?>
</div>

<!-- ホームに戻る -->
<div class="btn_home">
	<a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
</div>

<?php get_footer(); ?>