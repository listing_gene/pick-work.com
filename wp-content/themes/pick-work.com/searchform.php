<form method="get" role="search" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
	<div class="wrap_search">

	<!-- エリアドロップダウン -->
	<div class="selectbox sbmenu">
		<?php $terms=get_terms('area'); //タームの取得
		if($terms):
		?>
		<select class="form-control" name="area">
			<option value="">全エリア</option>
				<?php foreach($terms as $term):
				$term_slug = $term -> slug;
				$term_title = $term -> name;
				?>
			<option value="<?php echo esc_attr( $term_slug ); ?>"><?php echo esc_attr( $term_title ); ?></option>
				<?php endforeach; ?>
					</select>
				<?php endif; ?>
	</div>

	<!-- 職種ドロップダウン -->
	<div class="selectbox sbmenu">
		<?php $terms=get_terms('industry'); //タームの取得
		if($terms):
		?>
		<select class="form-control" name="industry">
			<option value="">全業種</option>
				<?php foreach($terms as $term):
				$term_slug = $term -> slug;
				$term_title = $term -> name;
				?>
			<option value="<?php echo esc_attr( $term_slug ); ?>"><?php echo esc_attr( $term_title ); ?></option>
				<?php endforeach; ?>
		</select>
		<?php endif; ?>
	</div>

	<!-- 支払い方法ドロップダウン -->
	<div class="selectbox sbmenu">
		<?php $terms=get_terms('payment'); //タームの取得
		if($terms):
		?>
		<select class="form-control" name="payment">
			<option value="">全支払い方法</option>
				<?php foreach($terms as $term):
				$term_slug = $term -> slug;
				$term_title = $term -> name;
				?>
			<option value="<?php echo esc_attr( $term_slug ); ?>"><?php echo esc_attr( $term_title ); ?></option>
				<?php endforeach; ?>
		</select>
		<?php endif; ?>
	</div>
</div>

<!-- メリットチェックボックス -->
<div class="wrap_condition">
	<div id="condition_title">
		<p>希望する項目条件をタップして絞り込み</p>
	</div>
	<ul id="condition">
		<?php $taxonomy_name = 'condition_details';
        $taxonomys = get_terms($taxonomy_name, $args);
		if ( !empty($taxonomys) && !is_wp_error($taxonomys)): //配列を受け取ることができ、値がエラーでなかった場合
		foreach ($taxonomys as $taxonomy):
    	?>
			<li class="<?php echo $taxonomy->slug; ?>">
				<input type="checkbox" name="condition_details[]" value="<?php echo $taxonomy->slug; ?>"/>
				<label></label>
            </li>
    	<?php endforeach; ?>
        <?php endif; ?>
	</ul>
</div>

<input type="hidden" name="s" id="s">
	
<!-- 検索ボタン -->
<div id="btn_search">
	<input type="submit" name="search" class="searchsubmit" value="<?php echo esc_attr_x( '検索する', 'submit button' ) ?>" />
</div>
</form>