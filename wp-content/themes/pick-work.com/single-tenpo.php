<?php get_header(); ?>

	<!-- パンくずリスト -->
	<div class="breadcrumb">
  <?php if(function_exists('bcn_display'))
  {
   bcn_display();
  }?>
	</div>

	<div class="shopdetail">
		<!-- 店舗名 -->
		<div class="shopname">
			<?php the_field("tenpo-name"); ?>
		</div>
		<!-- 店舗画像 -->
		<div class="shopimg">
			<div class="slider">
			<?php the_field("tenpo-slide"); ?>
			</div>
		</div>
		<!-- 基本情報 -->
		<div class="outline">
			<table>
				<tr>
					<th>体入時給</th>
					<td><?php echo nl2br( post_custom( 'hourly-salay1' ) ); ?></td>
				</tr>
				<tr>
					<th>最寄駅</th>
					<td><?php echo nl2br( post_custom( 'station' ) ); ?></td>
				</tr>
			</table>
		</div>
		<!-- 店舗説明 -->
		<div class="outline">
			<div class="about">
				<div class="about_title">店舗紹介</div>
				<p>
                    <?php the_field("introduction"); ?>
				</p>
			</div>
		</div>
		<div class="outline">
			<div class="about">
				<div class="about_title">仕事内容</div>
				<p>
                    <?php the_field("detail"); ?>
				</p>
			</div>
		</div>
		<!-- 応募ボタン -->
		<div class="subscription">
			<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_line_w.png" alt="LINEで応募する">
			</a>
			<a class="mail-apply" href="<?php echo home_url('contact')?>">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_mail_w.png" alt="メールで応募する">
			</a>
		</div>

		<!-- 店舗情報 -->
		<div class="shopdetail_label">
			店舗情報
		</div>
		<div class="shopinfo">
			<table>
				<tr>
					<th>店名</th>
					<td><?php the_field("tenpo-name"); ?></td>
				</tr>
				<tr>
					<th>住所</th>
					<td>
					<?php echo nl2br( post_custom( 'address' ) ); ?>
					</td>
				</tr>
				<tr>
					<th>業種</th>
					<td>
					<?php echo nl2br( post_custom( 'industry' ) ); ?>
					</td>
				</tr>
				<tr>
					<th>応募資格</th>
					<td>
                            <?php echo nl2br( post_custom( 'application' ) ); ?>
					</td>
				</tr>
				<tr>
					<th>定休日</th>
					<td>
					<?php echo nl2br( post_custom( 'holiday' ) ); ?>
						</ul>
					</td>
				</tr>
				<tr>
					<th>営業時間</th>
					<td>
					<?php echo nl2br( post_custom( 'business-hours' ) ); ?>
						</ul>
					</td>
				</tr>
				<tr>
					<th>給与支払</th>
					<td>
					<?php echo nl2br( post_custom( 'payment' ) ); ?>
					</td>
				</tr>
				<tr>
					<th>体入時給</th>
					<td><?php echo nl2br( post_custom( 'hourly-salay1' ) ); ?>
</td>
				</tr>
				<tr>
					<th>本入時給</th>
					<td><?php echo nl2br( post_custom( 'hourly-salay2' ) ); ?>
</td>
				</tr>
				<tr>
					<th>その他待遇</th>
					<td>
					<?php echo nl2br( post_custom( 'treatment' ) ); ?>
					</td>
				</tr>
			</table>
		</div>
		<!-- 応募ボタン -->
		<div class="subscription">
			<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_line_w.png" alt="LINEで応募する">
			</a>
			<a class="mail-apply" href="<?php echo home_url('/contact'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_mail_w.png" alt="メールで応募する">
			</a>
		</div>
	</div>
		
	<!-- スマホ用レコメンド -->
	<div class="recommended">
		<h2>このお店を見た人はこんなお店も見ています</h2>
		<div class="recommended-list">
		<?php $arg = array(
        'posts_per_page'   => 3, //表示件数
        'post_type'    => 'tenpo',  //カスタム投稿名
        'orderby'          => 'rand',  //ランダム
        'meta_key' => 'tenpo-pickup', //カスタムフィールドのキー
        'meta_value' => true, //カスタムフィールドの値
          );
		  $my_query = new WP_Query($arg);
		  if ($my_query->have_posts()) : while ($my_query->have_posts()) : $my_query->the_post();
		  ?>
			<div class="recommended_item">
				<div class="recommended_item_top">
				<div class="reco_left">
					<!-- 店舗画像 -->
					<a href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
				</div>
				<div class="reco_right">
					<!-- 店舗名 -->
					<div class="tenpo_name">
						<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
					</div>
					<!-- 店舗説明 -->
					<div class="tenpo_comment">
						<?php the_field("tenpo-comment"); ?>
					</div>
				</div>
				</div>
		<!-- 基本情報 -->
		<div class="info">
		<ul class="option op01">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-area' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-time' ) ); ?></span></li>
		</ul>
		<ul class="option op02">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-salay' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-payment' ) ); ?></span></li>
		</ul>
		</div>
		<!-- メリット -->
<ul id="condition">
            <?php $taxonomy_name = 'condition_details';
            $taxonomys = get_terms($taxonomy_name, $args);
            if ( !empty($taxonomys) && !is_wp_error($taxonomys)): //配列を受け取ることができ、値がエラーでなかった場合
                foreach ($taxonomys as $taxonomy):
			?>
			<?php if(has_term($taxonomy->slug, 'condition_details')): ?>
			<li class="">
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
				<?php else: ?>
			<li class="">
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
				<?php endif; ?>
				<?php endforeach; ?>
				
                <?php endif; ?>
</ul>
	<!-- 詳細を見る -->
	<div class="more-btn">
	<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
	</div>
			</div>
	
	<!-- PC用レコメンド -->
	<div class="recommended_item_pc">
		<div class="reco_left">
			<!-- 店舗画像 -->
			<a href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
		</div>
		<div class="reco_right">
			<!-- 店舗名 -->
			<div class="tenpo_name">
				<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
			</div>
			<!-- 店舗説明 -->
			<div class="tenpo_comment">
				<?php the_field("tenpo-comment"); ?>
			</div>
		<!-- 基本情報 -->
		<div class="info">
		<ul class="option op01">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-area' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-time' ) ); ?></span></li>
		</ul>
		<ul class="option op02">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-salay' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-payment' ) ); ?></span></li>
		</ul>
		</div>
		<!-- メリット -->
<ul id="condition">
            <?php $taxonomy_name = 'condition_details';
            $taxonomys = get_terms($taxonomy_name, $args);
            if ( !empty($taxonomys) && !is_wp_error($taxonomys)): //配列を受け取ることができ、値がエラーでなかった場合
                foreach ($taxonomys as $taxonomy):
			?>
			<?php if(has_term($taxonomy->slug, 'condition_details')): ?>
			<li class="">
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
				<?php else: ?>
			<li class="">
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
				<?php endif; ?>
				<?php endforeach; ?>
				
                <?php endif; ?>
</ul>
	<!-- 詳細を見る -->
	<div class="more-btn">
	<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る&nbsp;></a>
	</div>
		</div>
	</div>

	<?php endwhile; endif; wp_reset_postdata(); ?>
		</div>
	</div>

  	<!-- aimaリンクバナー -->
  	<div class="aima-banner">
	<p>▼全国展開！業界No1&thinsp;ギャラ飲みならaima</p>
	  <a href="https://aima-match.com/cast_pbqsrf/"><img src="<?php bloginfo('template_url'); ?>/img/aima_bnr.png" alt="居酒屋・バー・カラオケなど行き慣れた店舗で合流可能。ギャラ飲みならaima。LINE友達追加で簡単登録"></a>
	  <small>※周囲に知られたり勝手に投稿されたりすることは一切ありません</small>
	  </div>

	<!-- 店舗一覧に戻る -->
	<?php
	//前ページのURIを取得する
	$prev_url = $_SERVER['HTTP_REFERER'];
	//前ページがタームの一覧ページ、店舗一覧ページ以外の場合、店舗一覧ページに飛ばす
	if(strpos($prev_url, 'pick-work.com/area/') === false && $prev_url !== "http://pick-work.com/tenpo/" && $prev_url !== "http://pick-work.com/tenpo/") {
		$prev_url = "http://pick-work.com/tenpo/";
	}
	?>
	<div class="btn_home">
		<a href="<?php echo $prev_url;?>" class="btn_border">店舗一覧に戻る</a>
    </div>

<?php get_footer(); ?>
