<?php get_header(); ?>

<!-- タイトル画像 -->
<div class="page-heading">
	<img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_top.png" alt="">
	<h1>お問い合せ</h1>
	<img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_bottom.png" alt="">
</div>

<!-- テキスト -->
<div class="contact">
	<div class="contact_text">
		<p>応募とお問い合わせはこちらから。</p>
		<p>以下項目を入力いただき、「送信」ボタンを押してください。</p>
	</div>
	<!-- Contact Form 7プラグインショートコード -->
	<?php echo do_shortcode('[contact-form-7 id="785" title="お問い合わせフォーム"]') ?>
</div>

<!-- ホームに戻る -->
<div class="btn_home">
	<a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
</div>

<?php get_footer(); ?>