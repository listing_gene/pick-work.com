<?php get_header(); ?>

<?php
    if(have_posts()):
        while (have_posts()):
            the_post();
?>
<h1><article if="post-<?php the_ID(); ?>" <?php  post_class('page'); ?>></h1>
<?php
	the_content();
?>
	</article>
<?php
	endwhile;
endif;
?>

<?php get_footer(); ?>