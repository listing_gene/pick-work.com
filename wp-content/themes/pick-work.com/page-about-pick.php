<?php get_header(); ?>

<!-- タイトル画像 -->
<div class="page-heading">
    <img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_top.png" alt="">
    <h1>Pickとは</h1>
    <img class="frame" src="<?php bloginfo('template_url'); ?>/img/frame_bottom.png" alt="">
</div>

<!-- Pickとはのイメージ画像 -->
<div class="about-pick">
    <div class="image">
        <img class="about_pick_mb" src="<?php bloginfo('template_url'); ?>/img/about_pick.png" alt="ピックの運営のことや、コンセプトなどのご紹介">
        <img class="about_pick_pc" src="<?php bloginfo('template_url'); ?>/img/about_pick_pc.jpg" alt="ピックの運営のことや、コンセプトなどのご紹介">
    </div>

    <!-- 本文 -->
    <div class="pick_text">
        <p>ナイトワークに関する総合情報を発信するポータルサイトです。</p>
        <p>従来の求人サイトは求人要項だけがまとめられているものが多かったですが、Pickは口コミや実際に店舗に出向き集めた情報など、従来の求人サイトでは見れない情報を発信します。</p>
        <p>「ナイトワークで働いている、働きたい全ての女性の味方」というコンセプトで、役立つ情報をお届けする新しいナイトワーク情報サイトです。</p>
        <p>Pickは「会員制ラウンジ」「高級クラブ」「有名キャバクラ」を中心に都内全域に提携店舗があり、特に西麻布や恵比寿・六本木などの港区界隈に特化している情報サイトです。</p>
        <p>ナイトワーク(ラウンジ・キャバクラ・クラブ・ガールズバー・スナック)を全てカバーしており、現在は求人を中心とした情報サイトとして【店舗情報の発信】と【ナイトワーク求人】を行っています。</p>
    </div>
</div>

<!-- ホームに戻る -->
<div class="btn_home"><a class="btn_border" href="<?php echo home_url(); ?>">ホームに戻る</a></div>

<?php get_footer(); ?>