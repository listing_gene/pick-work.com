<?php get_header(); ?>

<!-- パンくずリスト -->
<div class="breadcrumb">
	<?php if(function_exists('bcn_display'))
	{
	bcn_display(); } ?>
</div>

<div class="arch-area">
	<!-- 条件検索表示 -->
	<?php get_search_form(); ?>
</div>

<!-- タイトル画像 -->
<div class="title_common">
	<img src="<?php bloginfo('template_url'); ?>/img/title_top.png" alt="">
	<h2>店舗一覧</h2>
	<img src="<?php bloginfo('template_url'); ?>/img/title_bottom.png" alt="">
</div>

<!-- 件数表示 -->
<div class="posts">
	<p><?php
	if ( have_posts() ) : 
	my_result_count();
	while ( have_posts() ) :
	the_post();
	/* do stuff */
	endwhile;
	else :
	/* Nothing Found */
	endif; ?></p>
</div>

<div class="shoplist_wrapper">
	<?php 
	if (have_posts()) :
	while (have_posts()) :
		the_post(); ?>
    
	<!-- ループ開始 -->
	<!-- スマホ用 -->
	<div class="shoplist">
		<div class="shoplist_top">
			<!-- 店舗名 -->
			<div class="shopname">
				<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
			</div>
		<!-- 所属タグ -->
		<div class="terms">
			<?php $terms = get_the_terms($post->ID,'area');
				foreach( $terms as $term ) {
				echo '<a class="side_area" href="'.get_term_link($term->slug, 'area').'">'.$term->name.'</a>'; } ?>
			<?php $terms = get_the_terms($post->ID,'industry');
				foreach( $terms as $term ) {
				echo '<a class="side_industry" href="'.get_term_link($term->slug, 'industry').'">'.$term->name.'</a>'; } ?>
		</div>
		<!-- 店舗画像 -->
		<a href="<?php the_permalink(); ?>"><img class="shoplist_image_archive" src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
		<!-- 店舗説明 -->
		<p><?php the_field("tenpo-comment"); ?></p>
		</div>
		<!-- 基本情報 -->
		<div class="info">
		<ul class="option op01">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-area' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-time' ) ); ?></span></li>
		</ul>
		<ul class="option op02">
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-salay' ) ); ?></span></li>
			<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-payment' ) ); ?></span></li>
		</ul>
		</div>
		<!-- メリット -->
		<ul id="condition">
            <?php $taxonomy_name = 'condition_details';
            $taxonomys = get_terms($taxonomy_name, $args);
            if ( !empty($taxonomys) && !is_wp_error($taxonomys)):
                foreach ($taxonomys as $taxonomy):
			?>
			<?php if(has_term($taxonomy->slug, 'condition_details')): ?>
			<li class="">
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
				<?php else: ?>
			<li class="">
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
			<?php endif; ?>
			<?php endforeach; ?>	
            <?php endif; ?>
		</ul>
		<!-- 応募ボタン -->
		<div class="apply">
			<div class="apply_line">
			<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_line_w.png" alt="LINEで応募する">
			</a>
			</div>
			<div class="apply_mail">
			<a class="mail-apply" href="<?php echo home_url('/contact'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_mail_w.png" alt="メールで応募する">
			</a>
			</div>
		</div>
		<!-- 詳細を見る -->
		<div class="more-btn">
		<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る<span>&nbsp;&nbsp;></span></a>
		</div>
	</div>

<!-- PC用 -->
<div class="shoplist_pc">
	<div class="shoplist_top">
		<!-- 店舗名 -->
		<div class="shopname">
			<a href="<?php the_permalink(); ?>"><?php the_field("tenpo-name"); ?></a>
		</div>
		<!-- 所属タグ -->
		<div class="terms">
			<?php $terms = get_the_terms($post->ID,'area');
				foreach( $terms as $term ) {
				echo '<a class="side_area" href="'.get_term_link($term->slug, 'area').'">'.$term->name.'</a>'; } ?>
			<?php $terms = get_the_terms($post->ID,'industry');
				foreach( $terms as $term ) {
				echo '<a class="side_industry" href="'.get_term_link($term->slug, 'industry').'">'.$term->name.'</a>'; } ?>
		</div>
	</div>
	<div class="shoplist_left">
		<a class="top_img" href="<?php the_permalink(); ?>"><img src="<?php the_field("tenpo-img"); ?>" alt="<?php the_field("tenpo-name"); ?>"></a>
		<!-- 応募ボタン -->
		<div class="apply">
			<div class="apply_line">
			<a class="line-apply" onclick="return gtag_report_conversion('https://line.me/R/ti/p/%40748oixwk')">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_line_w.png" alt="LINEで応募する">
			</a>
			</div>
			<div class="apply_mail">
			<a class="mail-apply" href="<?php echo home_url('/contact'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/img/btn_mail_w.png" alt="メールで応募する">
			</a>
			</div>
		</div>
	</div>
	<div class="shoplist_right">
		<!-- 店舗説明 -->
		<p><?php the_field("tenpo-comment"); ?></p>
		<!-- 基本情報 -->
		<div class="info">
			<ul class="option op01">
				<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_area.png" alt="場所"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-area' ) ); ?></span></li>
				<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_time.png" alt="営業時間"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-time' ) ); ?></span></li>
			</ul>
			<ul class="option op02">
				<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_money.png" alt="時給"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-salay' ) ); ?></span></li>
				<li><span class="op_img"><img src="<?php bloginfo('template_url'); ?>/img/icon_shoplist_salay.png" alt="支払い方法"></span><span class="op_text"><?php echo nl2br( post_custom( 'top-payment' ) ); ?></span></li>
			</ul>
		</div>
		<!-- メリット -->
		<ul id="condition">
		<?php $taxonomy_name = 'condition_details';
			$taxonomys = get_terms($taxonomy_name, $args);
			if ( !empty($taxonomys) && !is_wp_error($taxonomys)):
			foreach ($taxonomys as $taxonomy): ?>
			<?php if(has_term($taxonomy->slug, 'condition_details')): ?>
			<li>
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_aft_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
			<?php else: ?>
			<li>
				<img src="<?php bloginfo('template_url'); ?>/img/icon_search_list_<?php echo $taxonomy->slug; ?>.png" alt="<?php echo $taxonomy->name; ?>">
			</li>
			<?php endif; ?>
			<?php endforeach; ?>
			<?php endif; ?>
		</ul>
		<!-- 詳細を見る -->
		<div class="more-btn top-more-btn">
			<a href="<?php the_permalink(); ?>" class="btn_detail">詳細を見る<span>&nbsp;&nbsp;></span></a>
		</div>
	</div>
</div>

<!-- ループ終了 -->
<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata(); ?>
</div>

<!-- ページ送り -->
<div class="pagination">
    <?php wp_pagenavi(); ?>
</div>
		
<!-- ホームに戻る -->
<div class="btn_home">
	<a href="<?php echo home_url(); ?>" class="btn_border">ホームに戻る</a>
</div>

<?php get_footer(); ?>