<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pick_wpdb' );

/** MySQL database username */
define( 'DB_USER', 'pick_wpdb' );

/** MySQL database password */
define( 'DB_PASSWORD', 'eu74im8h8g6r' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '11H6kIqYw8H6zQJEBsPuMMswdikQ8sYbDw0R2EcUL8njF3bHpKiXBbXIqkeHnyXwsM5MI70kSmzOOTozYP+SGQ==');
define('SECURE_AUTH_KEY',  'fnmAPfrm+R2yO1OiZOvIJYNOKNKV/+yNxdW2pYJ9Bth6SIxZ3SWmrkXj7qrIFZWyGPSRm9R46KC6IHcLCSiaCw==');
define('LOGGED_IN_KEY',    'z/8dBKNVK3uVqoslNy+rBS3SmUSW5uKU/gJlAztDdDc2J279yUWSCvG7J8M6uoSG565OYLTCYzwnJ6mJ1TvhDg==');
define('NONCE_KEY',        'ehb8mlaLmdIYuK3R8Rtipp6+ie4qTRiRd4rYizJ99XhXBJVlqyESkr1MzIX69kn4YP7VGV7i8hS8kliJnNu2ZQ==');
define('AUTH_SALT',        'DyFtEJHYS3S2JdJ0AAIZXknifnzFELL58kkQCnstIb2o1vV6sVW+9OvDCLOt3aRIB6Klcq+XnLpr88IPO1MBeg==');
define('SECURE_AUTH_SALT', 'dPut43u04bh8HqU4XuVjFLBLCujQrhEofqWIhQpSKvONPKYgqwkUAXBpVhVgQsNkyV/t0AvShWKNikEvuFEUNw==');
define('LOGGED_IN_SALT',   'Y6AeK1n9qJxm1h9bAhWAePhovA43DgFBt1OKbh9fAwkmSgLXA15OAMT8NRuCkXWNm/0aWmh1Usl/gLVqsa5eEw==');
define('NONCE_SALT',       'zmpTQQ+Cpz53VZvfENRYR1KLoNm7/sRZZ5wgWk21ne0Et2KT7PXrPZkdOBwk2jxb5H7zZLNMe/WtN2Ch/mug4g==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
